package ClassTwoQueue;

public class Main {
    public static void main(String[] args) {
        SetCola<Integer> setCola = new SetCola<>();
        setCola.add(1);
        setCola.add(2);
        setCola.add(3);
        setCola.add(4);
        setCola.add(1);
        System.out.println(setCola);
        setCola.add(3);
        setCola.add(4);
        setCola.add(5);
        setCola.add(1);
        System.out.println(setCola);
    }
}
