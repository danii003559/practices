package ClassTwoQueue;


public class SetCola<T extends Comparable<T>>{
    private Node <T> headNode;
    private Node<T> lastNode;

    public void add(T data) {
        if(verifyIsThere(data)) {
            if (headNode == null) {
                headNode = new Node<>(data);
                lastNode = headNode;
            } else {
                lastNode.setNext(new Node<>(data));
                lastNode = lastNode.getNext();
            }
        }
    }
    private boolean verifyIsThere(T t){
        boolean available = true;
        for(Node<T> node = headNode; node!=null; node =node.getNext()){
            if(node.getData().compareTo(t)== 0){
                available = false;
                break;
            }
        }
        return available;
    }
    public String toString() {
        return headNode.toString();
    }
}

