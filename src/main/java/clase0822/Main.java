package clase0822;

import clase0822.recursividad.Contador;
import clase0822.recursividad.Infinito;
import clase0822.recursividad.LlamadasEncadenadas;
import clase0822.recursividad.RecursividadvsIteracion;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion: Como bien esta nombrada la clase Infinito, contiene un metodo recursivo infinito,  y lanza una excepcion en tiempo de ejecucion de tipo java.lang.StackOverflowError,
        // es decir un desbordamiento de la pila "stack", en el cual los metodos ejecutados estan ocupando demasiado espacio en memoria
        // y esto es mas comun que suceda en metodos recursivos por que no se esta liberando el espacio, es mas  cada vez que el metodo se vuelve a ejecutar aumenta el espacio usado.
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion:Es un metodo recursivo  que recive un parametro de tipo entero llamado "contador", este se vuelve a ejecutar a si mismo con el parametro restado en una unidad
        //siempre y cuando se cumpla la condicion de que el parametro sea mayor que 0 , ademas antes de comparar el parametro lo imprime para asi tener la secuencia de numeros
        //simulando un conteo hasta el parametro inicial añadido, cuando ya no se cumpla la condicion del (parametro>0) recien se liberara el espacio que se esta usando.
        //Contador.contarHasta(100);
        long timeStart, timeFinished;
        timeStart  = System.nanoTime();
        System.out.println(RecursividadvsIteracion.factorialIter(5));
        timeFinished = System.nanoTime();
        System.out.println("Tiempo de Ejecucion con Iteraciones : "+(timeFinished-timeStart));
        timeStart  = System.nanoTime();
        System.out.println(RecursividadvsIteracion.factorialRecu(5));
        timeFinished = System.nanoTime();
        System.out.println("Tiempo de Ejecucion con Recursividad : "+(timeFinished-timeStart));
    }
}
