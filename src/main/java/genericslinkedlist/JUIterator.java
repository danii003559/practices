package genericslinkedlist;

import java.util.Iterator;

public class JUIterator<T> implements Iterator<T> {
    private Node nodeList;

    @Override
    public boolean hasNext() {
        return nodeList.getNext()!= null;
    }

    @Override
    public T next() {
        T data = (T) nodeList.getData();
        nodeList = nodeList.getNext();
        return data;
    }

    public void setNode(Node node) {
        this.nodeList = node;
    }
}
