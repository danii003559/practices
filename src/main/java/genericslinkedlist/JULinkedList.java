package genericslinkedlist;

import java.util.*;

public class JULinkedList<T> implements List<T> {

    private Node rootNode;
    private Integer sizeList = 0;

    public JULinkedList(T... nodes) {
        for (T node : nodes) {
            add(node);
        }
    }

    @Override
    public int size() {
        return sizeList;
    }
    @Override
    public boolean isEmpty() {
        return rootNode == null;
    }

    @Override
    public boolean contains(Object o) {
        boolean availableContent = false;
        Node nodeSearched = rootNode;
        while (nodeSearched != null) {
            if (nodeSearched.getData().equals(o)) {
                availableContent = true;
                break;
            }
            nodeSearched = nodeSearched.getNext();
        }
        return availableContent;
    }

    @Override
    public Iterator<T> iterator() {
        JUIterator<T> iterator = new JUIterator<>();
        iterator.setNode(rootNode);
        return iterator;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[sizeList];
        Node node = rootNode;
        for (int i = 0; i < array.length; i++) {
            array[i] = node.getData();
            node = node.getNext();
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
         a = (T1[]) new Object[sizeList];
        Node<T> node = rootNode;
        for (int i = 0; i < a.length; i++) {
            a[i] = (T1) node.getData();
            node = node.getNext();
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if (rootNode == null) {
            rootNode = new Node(t);
        } else {
            Node node = getLastNode();
            node.setNext(new Node(t));
        }
        sizeList++;
        return true;
    }
    private Node getLastNode(){
        Node ultimo = rootNode;
        while(ultimo.getNext()!=null){
            ultimo= ultimo.getNext();
        }
        return ultimo;
    }
    @Override
    public boolean remove(Object o) {
        boolean availableRemove = false;
        if(o.equals(rootNode.getData())){
            rootNode = rootNode.getNext();
            sizeList--;
            availableRemove = true;
        }else{
            for (Node nodeIndex = rootNode; nodeIndex.getNext()!= null; nodeIndex = nodeIndex.getNext()) {
                if (o.equals(nodeIndex.getNext().getData())) {
                    sizeList--;
                    availableRemove = true;
                    nodeIndex.setNext(nodeIndex.getNext().getNext());
                    break;
                }
            }
        }
        return availableRemove;
    }
    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        rootNode = null;
        sizeList = 0;
    }

    @Override
    public T get(int index) {
        Node nodeSearched = rootNode;
        for (int i = 0; i <= index; i++) {
            if (index == i) {
                break;
            }
            nodeSearched = nodeSearched.getNext();
        }
        return (T)nodeSearched.getData();
    }

    @Override
    public T set(int index, T element) {
        Node nodePrevious = rootNode, newNode = new Node(element);
        T dataReturned = null;
        if (index != 0) {
            for (int i = 0; i < index; nodePrevious = nodePrevious.getNext(), i++) {
                if (i == index - 1) {
                    dataReturned = (T) nodePrevious.getNext().getData();
                    newNode.setNext( nodePrevious.getNext().getNext());
                    nodePrevious.setNext( newNode);
                    break;
                }
            }
        } else {
            dataReturned = (T) rootNode.getData();
            newNode.setNext(rootNode.getNext());
            rootNode = newNode;
        }
        return dataReturned;
    }

    @Override
    public void add(int index, T element) {
        Node node = rootNode,nodeAux;
        if(index==0){
            rootNode = new Node(element,node);
            sizeList++;
        }else{
            for (int i = 1; i<=index; node = node.getNext(), i++) {
                if (i == index) {
                    nodeAux = node.getNext();
                    node.setNext(new Node(element,nodeAux));
                    sizeList++;
                }
            }
        }
    }

    @Override
    public T remove(int index) {
        Node nodePrevious = rootNode;
        T dataReturned = null;
        if (index != 0) {
            for (int i = 1; i <= index; nodePrevious = nodePrevious.getNext(), i++) {
                if (i == index) {
                    sizeList--;
                    dataReturned = (T) nodePrevious.getNext().getData();
                    nodePrevious.setNext(nodePrevious.getNext().getNext());
                    break;
                }
            }
        } else {
            sizeList--;
            dataReturned = (T) rootNode.getData();
            rootNode = rootNode.getNext();
        }
        return dataReturned;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1, counter = 0;
        Node nodeSearched = rootNode;
        while (nodeSearched != null) {
            if (nodeSearched.getData().equals(o)) {
                index = counter;
                break;
            }
            counter++;
            nodeSearched = nodeSearched.getNext();
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1, counter = 0;
        Node nodeLastSearched = rootNode;
        while (nodeLastSearched != null) {
            if (nodeLastSearched.getData().equals(o)) {
                index = counter;
            }
            counter++;
            nodeLastSearched = nodeLastSearched.getNext();
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        JULinkedList<T> newList = new JULinkedList<>();
        Node node = rootNode;
        if(fromIndex!=toIndex){
            for(int i = 0; i<toIndex;i++,node = node.getNext()){
                if(i==fromIndex){
                    break;
                }
            }
            newList.rootNode = node;
            newList.sizeList = toIndex-fromIndex;
        }
        return newList;
    }
}
