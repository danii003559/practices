package implementation_interfaz;

public class DoublyLinkedList<T extends Comparable<T>> implements List<T> {
    private Node<T> nodeHead;
    private Node<T> nodeTail;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return nodeHead == null;
    }

    @Override
    public boolean add(T data) {
        Node<T> newNode = new Node<>(data);
        if (nodeHead == null) {
            nodeHead = newNode;
        } else {
            nodeTail.setNext(newNode);
            newNode.setPrevious(nodeTail);
        }
        size++;
        nodeTail = newNode;
        return true;
    }

    @Override
    public boolean remove(T data) {
        boolean availableRemove = false;
        for (Node<T> node = nodeHead; node != null; node = node.getNext()) {
            if (node.getData().compareTo(data) == 0) {
                if (node.getNext() == null) {
                    comparePrevious(node);
                } else if (node.equals(nodeHead)) {
                    node.getNext().setPrevious(null);
                    nodeHead = node.getNext();
                } else {
                    node.getPrevious().setNext(node.getNext());
                    node.getNext().setPrevious(node.getPrevious());
                }
                availableRemove = true;
                size--;
                break;
            }
        }
        return availableRemove;
    }

    private void comparePrevious(Node<T> node) {
        if (node.getPrevious() == null) {
            nodeHead = null;
        } else {
            nodeTail = node.getPrevious();
            nodeTail.setNext(null);
        }
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node<T> node = nodeHead;
        for (int i = 0; i <= index; node = node.getNext(), i++) {
            if (i == index) {
                break;
            }
        }
        return node.getData();
    }

    @Override
    public void xchange(int x, int y) {
        Node<T> nodeTraveled = nodeHead,nodeX = null;
        for (int i = 0; nodeTraveled != null; nodeTraveled = nodeTraveled.getNext(), i++) {
            if (i == x) {
                nodeX = nodeTraveled;
            }
            if (i == y) {
                if (nodeTraveled.getPrevious().equals(nodeX)) {
                    changeToNext(nodeX, nodeTraveled);
                } else {
                    longChange(nodeX, nodeTraveled);
                }
            }

        }
    }

    /**
     *This method is responsible for switching nodes over long distances.
     * @param nodeX node in the X index.
     * @param nodeY node in the Y index.
     */
    private void longChange(Node<T> nodeX, Node<T> nodeY) {
        Node<T> nodeAux = nodeX.getNext();
        nodeX.setNext(nodeY.getNext());
        if (nodeX.getNext() != null) {
            nodeX.getNext().setPrevious(nodeX);
        }
        nodeY.setNext(nodeAux);
        nodeAux.setPrevious(nodeY);
        nodeAux = nodeY.getPrevious();
        nodeY.setPrevious(nodeX.getPrevious());
        if (nodeY.getPrevious() != null) {
            nodeY.getPrevious().setNext(nodeY);
        } else {
            nodeHead = nodeY;
        }
        nodeAux.setNext(nodeX);
        nodeX.setPrevious(nodeAux);
    }

    /**
     *This method is in charge of changing the nodes that are together.
     * @param nodeX node in the X index.
     * @param nodeY node in the Y index.
     */
    private void changeToNext(Node<T> nodeX, Node<T> nodeY) {
        nodeX.setNext(nodeY.getNext());
        if (nodeY.getNext() != null) {
            nodeY.getNext().setPrevious(nodeX);
        }
        nodeY.setNext(nodeX);
        nodeY.setPrevious(nodeX.getPrevious());
        nodeX.setPrevious(nodeY);
        if (nodeY.getPrevious() != null) {
            nodeY.getPrevious().setNext(nodeY);
        } else {
            nodeHead = nodeY;
        }
    }
    @Override
    public String toString() {
        return "(" + size + ") -> " + nodeHead;
    }
}
