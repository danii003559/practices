package implementation_interfaz;

public class Node<T> {
    private final T data;
    private Node<T> next;
    private Node<T> previous;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> next, Node<T> previous) {
        this.data = data;
        this.next = next;
        this.previous = previous;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public Node<T> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<T> previous) {
        this.previous = previous;
    }

    public String toString() {
        String demonstrable;
        if (previous != null) {
            demonstrable = "{ Previous : " + previous.getData();
        } else {
            demonstrable = "{ Previous : " + null;
        }
        demonstrable += " Data : " + data + "  Next : " + next + "}";
        return demonstrable;
    }
}
