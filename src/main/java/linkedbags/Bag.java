package linkedbags;

public interface Bag<T extends Comparable<T>> {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    void mergeSort();
}