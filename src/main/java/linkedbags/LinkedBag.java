package linkedbags;

public class LinkedBag<T extends Comparable<T>> implements Bag<T> {
    private int size;
    private Node<T> root;

    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;
        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    @Override
    public void selectionSort() {
        Node nodeX, nodeY, nodeAux, nodeBefore, nodeXBefore = null;
        for (nodeX = root; nodeX.getNext() != null; nodeXBefore = nodeX, nodeX = nodeX.getNext()) {
            nodeY = nodeX.getNext();
            nodeBefore = nodeX;
            while (nodeY != null) {
                if (((T) nodeX.getData()).compareTo((T) nodeY.getData()) < 0) {
                    nodeAux = nodeY;
                    nodeY = nodeY.getNext();
                    if (nodeX.equals(root)) {
                        if (root.equals(nodeBefore)) {
                            nodeAux.setNext(root);
                            root.setNext(nodeY);
                        } else {
                            nodeAux.setNext(nodeX.getNext());
                            nodeX.setNext(nodeY);
                            nodeBefore.setNext(nodeX);
                            nodeBefore = nodeBefore.getNext();
                        }
                        nodeX = nodeAux;
                        root = nodeAux;
                    } else {
                        if (nodeX.equals(nodeBefore)) {
                            nodeX.setNext(nodeY);
                            nodeAux.setNext(nodeX);
                            nodeXBefore.setNext(nodeAux);
                        } else {
                            nodeAux.setNext(nodeX.getNext());
                            nodeX.setNext(nodeY);
                            nodeBefore.setNext(nodeX);
                            nodeXBefore.setNext(nodeAux);
                            nodeBefore = nodeBefore.getNext();
                        }
                        nodeX = nodeAux;
                    }
                } else {
                    nodeY = nodeY.getNext();
                    nodeBefore = nodeBefore.getNext();
                }
            }
        }
    }

    @Override
    public void bubbleSort() {
        boolean availableSort;
        Node nodeActually, nodeBefore, nodeAux, nodeBeforeX = null;
        do {
            availableSort = true;
            nodeActually = root.getNext();
            nodeBefore = root;
            while (nodeActually != null) {
                if (((T) nodeBefore.getData()).compareTo((T) nodeActually.getData()) < 0) {
                    nodeAux = nodeActually;
                    nodeActually = nodeActually.getNext();
                    if (nodeBefore.equals(root)) {
                        root.setNext(nodeActually);
                        nodeAux.setNext(root);
                        root = nodeAux;
                        nodeBeforeX = root;
                    } else {
                        nodeBefore.setNext(nodeActually);
                        nodeAux.setNext(nodeBefore);
                        nodeBeforeX.setNext(nodeAux);
                        nodeBeforeX = nodeBeforeX.getNext();
                    }
                    availableSort = false;
                } else {
                    if (nodeBefore.equals(root)) {
                        nodeBeforeX = root;
                    } else {
                        nodeBeforeX = nodeBeforeX.getNext();
                    }
                    nodeBefore = nodeActually;
                    nodeActually = nodeActually.getNext();
                }
            }
        } while (!availableSort);
    }

    public void mergeSort() {
        root = makeMergeSort(root, size);
    }

    private Node<T> makeMergeSort(Node<T> node, int sizeSequence) {
        Node<T> nodeMade;
        int sizeLeft, sizeRight;
        if (sizeSequence == 1) {
            nodeMade = node;
        } else {
            sizeLeft = sizeSequence / 2;
            sizeRight = sizeSequence - sizeLeft;
            Node<T> nodeRight = node, nodeLeft = node;
            for (int i = 0; i < sizeLeft; i++, node = nodeRight, nodeRight = nodeRight.getNext()) ;
            node.setNext(null);
            nodeLeft = makeMergeSort(nodeLeft, sizeLeft);
            nodeRight = makeMergeSort(nodeRight, sizeRight);
            nodeMade = combination(nodeLeft, nodeRight, sizeRight + sizeLeft);
        }
        return nodeMade;
    }

    private Node<T> combination(Node<T> nodeLeft, Node<T> nodeRight, int size) {
        Node<T> nodeCombined = null, nodeX, nodeY;
        int counter = 0;
        while (counter < size) {
            if (nodeLeft == null || nodeRight == null) {
                if (nodeLeft != null) {
                    nodeX = searchMinor(nodeLeft);
                    nodeCombined = joinNode(nodeCombined, nodeX);
                    nodeLeft = remove(nodeX.getData(), nodeLeft);
                } else {
                    nodeY = searchMinor(nodeRight);
                    nodeCombined = joinNode(nodeCombined, nodeY);
                    nodeRight = remove(nodeY.getData(), nodeRight);
                }
            } else {
                nodeX = searchMinor(nodeLeft);
                nodeY = searchMinor(nodeRight);
                if ((nodeX.getData()).compareTo(nodeY.getData()) > 0) {
                    nodeCombined = joinNode(nodeCombined, nodeY);
                    nodeRight = remove(nodeY.getData(), nodeRight);
                } else {
                    nodeCombined = joinNode(nodeCombined, nodeX);
                    nodeLeft = remove(nodeX.getData(), nodeLeft);
                }
            }
            counter++;
        }
        return nodeCombined;
    }

    private Node<T> joinNode(Node root, Node node) {
        node.setNext(root);
        root = node;
        return root;
    }

    private Node<T> searchMinor(Node<T> node) {
        Node nodeSearched = node;
        while (node != null) {
            if (((T) nodeSearched.getData()).compareTo(node.getData()) > 0) {
                nodeSearched = node;
            }
            node = node.getNext();
        }
        return new Node<>((T) nodeSearched.getData());
    }

    private Node<T> remove(Object o, Node<T> rootNode) {
        if ((rootNode.getData().equals(o))) {
            rootNode = rootNode.getNext();
        } else {
            for (Node nodeIndex = rootNode; nodeIndex.getNext() != null; nodeIndex = nodeIndex.getNext()) {
                if (o.equals(nodeIndex.getNext().getData())) {
                    nodeIndex.setNext(nodeIndex.getNext().getNext());
                    break;
                }
            }
        }
        return rootNode;
    }
}