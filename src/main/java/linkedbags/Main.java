package linkedbags;

public class Main {
    private static Bag<Integer> fillLinked(){
        Bag<Integer> linkedBag = new LinkedBag<>();
        int size = 30000;
        for(int i =0 ; i<30000;i++){
            linkedBag.add(Integer.valueOf((int)(Math.random() * size)));
        }
        return linkedBag;
    }
    public static void main(String[] args) {
        long startTime,endTime;
        Bag<Integer> linkedBagIntegers ;
        linkedBagIntegers = fillLinked();
        startTime = System.nanoTime();
        linkedBagIntegers.selectionSort();
        endTime = System.nanoTime();
        System.out.println("Elapsed Time SelectionSort : "+ convert( endTime-startTime));


        linkedBagIntegers = fillLinked();
        startTime = System.nanoTime();
        linkedBagIntegers.bubbleSort();
        endTime =System.nanoTime();
        System.out.println("Elapsed Time BubbleSort : "+ convert( endTime-startTime));


        linkedBagIntegers = fillLinked();
        startTime = System.nanoTime();
        linkedBagIntegers.mergeSort();
        endTime = System.nanoTime();
        System.out.println("Elapsed Time MargeSort : "+ convert( endTime-startTime));
    }
    public static double convert(long time){
        return (double)(time)/1000000;
    }
}
