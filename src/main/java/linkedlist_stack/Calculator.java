package linkedlist_stack;

import linkedlist_stack.operables.Operable;
import linkedlist_stack.operables.Powers;

/**
 * This class is in charge of correctly executing the sequence of operations to obtain the expected result.
 *
 * @author Daniel_Espinoza
 * @see Parser
 * @see Validator
 * @see Operations
 */
public class Calculator{
    private final Parser parser;
    private final Validator validator;
    private Stack<Character> stackClass;
    private Operable operable;
    Operations operations ;

    /**
     * This is a constructor method that initializes program attributes.
     */
    public Calculator() {
        parser = new Parser();
        validator = new Validator();
        stackClass = new Stack<>();
        operations = new Operations();
         operable= new Powers();
    }

    /**
     * This method calls the validateStack() method that validates the sequence of characters entered for the operation,
     * and is responsible for calling to simplify() method to start processing the sequence and execute it.
     *
     * @param operation input string.
     * @return result of Operation.
     */

    public long calculate(String operation) {
        validator.validateStack(parser.parseToStack(operation));
        stackClass = simplify(parser.parseToStack(operation));
        return parser.parseToInt(stackClass);
    }

    /**
     * This is a recursive method and aims to simplify the input string in order to process and execute it in a simpler way.
     * <p>
     * The main purpose of this function is to decompose the string to a string without any parenthesis so that it can be executed.
     *
     * @param characterStack stack of operation characters.
     * @return solution.
     */
    private Stack<Character> simplify(Stack<Character> characterStack) {
        stackClass = new Stack<>();
        int amountParenthesis = countParenthesisOpen(characterStack);
        characterStack = parser.revertStack(stackClass);
        Stack<Character> stackReverse = new Stack<>();
        if (amountParenthesis == 0) {
            return resolveOperation(characterStack);
        } else {
            Stack<Character> cadena = searchParenthesisPriority(characterStack, stackReverse);
            cadena = simplify(cadena);
            stackReverse = parser.revertStack(stackReverse);
            cadena = parser.joinStacks(stackReverse, cadena, characterStack);
            return simplify(parser.revertStack(cadena));
        }
    }

    /**
     * This method is in charge of operating on a string without any parenthesis until obtaining a result of only numeric type characters.
     *
     * @param characterStack stack of operation characters without brackets.
     * @return solution.
     */

    private Stack<Character> resolveOperation(Stack<Character> characterStack) {
        Stack<Character> stackReverse = new Stack<>();
        while (!isOnlyNumber(characterStack)) {
            characterStack = parser.revertStack(stackClass);
            int pri = searchMaxPriority(characterStack);
            characterStack = stackClass;
            int result = searchNumbersPriority(characterStack, stackReverse, pri);
            stackReverse = parser.revertStack(stackReverse);
            characterStack = parser.joinStacks(stackReverse, parser.parseIntToStack(result), characterStack);
        }
        characterStack = parser.revertStack(stackClass);
        return characterStack;
    }

    /**
     * This method checks that the stack only contains characters of numeric type.
     *
     * @param characterStack stack to check.
     * @return boolean value , true if there are only numeric characters ,and false if there are special characters.
     * @see Utils to special characters.
     */

    private boolean isOnlyNumber(Stack<Character> characterStack) {
        boolean onlyNumbers = true;
        char c;
        while (!characterStack.Empty()) {
            c = characterStack.pop();
            if (!Utils.isADigit(c)) {
                onlyNumbers = false;
            }
            stackClass.push(c);
        }
        return onlyNumbers;
    }

    /**
     * This method is in charge of looking for the operator with the highest priority in the stack to operate.
     *
     * @param operations stack of operating characters.
     * @return priority Level
     */

    private int searchMaxPriority(Stack<Character> operations) {
        int maxPriority = 0;
        char c;
        while (!operations.Empty()) {
            c = operations.pop();
            if (Utils.getIndexPriority(c) >= maxPriority) {
                maxPriority = Utils.getIndexPriority(c);
            }
            stackClass.push(c);
        }
        return maxPriority;
    }

    /**
     * This method is in charge of grouping and executing operations in their simplest form.
     *
     *for example
     *                   number , operator , number
     *            or
     *                        number ,operator      in case of the factorial.
     *
     * Ensures that the input stack has no brackets.
     * Also divides the stack into two ends of the operations to be performed.
     *
     * @param newStack stack principal.
     * @param stackReverse stack with of rest of operands.
     * @param priority level of operators priority.
     * @return solution.
     */

    private int searchNumbersPriority(Stack<Character> newStack, Stack<Character> stackReverse, int priority) {
        char c;
        int numberOne = 0, exponent = 0;
        while (!newStack.Empty()) {
            c = newStack.pop();
            stackReverse.push(c);
            if (Utils.getIndexPriority(c) == priority) {
                numberOne = executeSimpleOperation(newStack,stackReverse,numberOne,exponent,c);
                break;
            }
            if (Utils.isADigit(c)) {
                numberOne += (c - 48) * operable.makeOperation(10, exponent);
                exponent++;
            } else {
                numberOne = 0;
                exponent = 0;
            }
        }
        return numberOne;
    }

    /**
     *This method is in charge of giving parameters to the operate method of the Operations class
     *
     * @param stack character stack before operation.
     * @param stackReverse character stack after operation.
     * @param numberTwo number of the operation.
     * @param amountNumbers length numbers of numberOne.
     * @param operator type of operator.
     * @return number of solution.
     */
    private int  executeSimpleOperation(Stack<Character> stack,Stack<Character> stackReverse,int numberTwo,int amountNumbers, char operator){
       int  numberOne = bringNumber(stack);
        if (operator == '!') {
            stackReverse.pop();
            numberTwo = 0;
        } else {
            clearStack(stackReverse, amountNumbers);
        }
        return  operations.operate(numberOne, operator, numberTwo);
    }

    /**
     * This method returns the following numbers after an operator.
     *
     * @param stack character stack to operate.
     * @return numbers fined.
     */

    private int bringNumber(Stack<Character> stack) {
        int exponent = 0, number = 0;
        char c;
        while (!stack.Empty()) {
            c = stack.pop();
            if (Utils.isADigit(c)) {
                number += (c - 48) * operable.makeOperation(10, exponent);
                exponent++;
            } else {
                stack.push(c);
                break;
            }
        }
        return number;
    }

    /**
     *This method removes a certain number of characters from a stack.
     *
     * @param stack stack with the characters to be deleted.
     * @param amount number of characters to delete.
     */

    private void clearStack(Stack<Character> stack, long amount) {
        while (amount >= 0) {
            stack.pop();
            amount--;
        }
    }

    /**
     * This method takes care of dividing the pla in taking into account the square brackets.
     *
     * if the stack does not have square brackets, returns the same stack.
     * if the stack have brackets divide the stack in three parts
     *      *Characters before the brackets.
     *      *Characters into the brackets.
     *      *Characters after the brackets.
     *
     * @param newStack stack with characters to be split and will contain the characters after the brackets.
     * @param stackReverseStack Character stack before the brackets.
     * @return characters stack into the brackets.
     */

    private Stack<Character> searchParenthesisPriority(Stack<Character> newStack, Stack<Character> stackReverseStack) {
        char c;
        Stack<Character> cadena = new Stack<>();
        boolean yes = true;
        int amountParenthesis = 0;
        while (!newStack.Empty()) {
            c = newStack.pop();
            amountParenthesis += counterParenthesis(c);
            if (!yes && amountParenthesis == 0) {
                break;
            } else if ((yes && amountParenthesis == 1)) {
                yes = false;
            } else if (amountParenthesis == 0) {
                stackReverseStack.push(c);
            } else {
                cadena.push(c);
            }
        }
        return parser.revertStack(cadena);
    }

    /**
     *This method return -1 if parenthesis is closed and +1 if parenthesis is open.
     *
     * @param parenthesis
     * @return -1 or +1
     */

    private int counterParenthesis(char parenthesis) {
        int amount = 0;
        if (parenthesis == '(') {
            amount = 1;
        }
        if (parenthesis == ')') {
            amount = -1;
        }
        return amount;
    }

    /**
     *This method counts parenthesis into a stack.
     *
     * @param characterStack
     * @return number of open parentheses.
     */

    private int countParenthesisOpen(Stack<Character> characterStack) {
        int counter = 0;
        char c;
        while (!characterStack.Empty()) {
            c = characterStack.pop();
            if (c == '(') {
                counter++;
            }
            stackClass.push(c);
        }
        return counter;
    }
}
