package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import linkedlist_stack.operables.*;

/**
 *This class is in charge of executing the operations according to the indicated operator.
 *
 * @author Daniel_Espinoza
 */
public class Operations {

    /**
     * This method evaluates the operator given as a parameter and assigns the numbers a and b to an operation.
     *
     * @param a
     * @param operation
     * @param b
     * @return Operation Result.
     */
    public int  operate(int a, char operation, int b){
        long  result = 0;
        Operable operable;
        switch(operation){
            case '*':
                operable = new Multiplication();
                result = operable.makeOperation(a,b);
                break;
            case '+':
                  operable = new Sum();
                result = operable.makeOperation(a,b);
                break;
            case '^':
                  operable = new Powers();
                result = operable.makeOperation(a,b);
                break;
            case '!':
                operable = new Factorial();
                result = operable.makeOperation(a,b);
                break;
            default:
                break;
        }
        controlLimit(result);
        return (int)result;
    }

    /**
     *This method controls the exceptions with a result out of the limit.
     *
     * @param result
     */
    private void controlLimit(long result){
        if(result>Utils.LIMIT_NUMBER || result<0){
            throw new NumberOutOfRangeException("The operation exceeded the allowed integer number limit");
        }
    }

}
