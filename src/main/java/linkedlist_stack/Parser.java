package linkedlist_stack;

import linkedlist_stack.operables.Operable;
import linkedlist_stack.operables.Powers;

/**
 *This is in charge of to handle the stacks.
 *
 * @author Daniel_Espinoza
 */
public class Parser {
    private Operable operable = new Powers();
    /**
     * This method is in charge of converting a String into a stack.
     * @param operation input string.
     * @return new stack.
     */
    public Stack parseToStack(String operation) {
        char a;
        Stack<Character> newStack = new Stack<>();
        for (int i = operation.length() - 1; i >= 0; i--) {
            a = operation.charAt(i);
            if (a != ' ') {
                newStack.push(a);
            }
        }
        return newStack;
    }

    /**
     *This method turns a stack upside down.
     *
     * @param stackWillChanged
     * @return revert stack.
     */

    public Stack<Character> revertStack(Stack<Character> stackWillChanged) {
        Stack<Character> stackReverted = new Stack<>();
        while (!stackWillChanged.Empty()) {
            stackReverted.push(stackWillChanged.pop());
        }
        return stackReverted;
    }

    /**
     *This method joins 3 Stacks into one.
     *
     * @param rightStack
     * @param centerStack
     * @param leftStack
     * @return the new Stack
     */
    public Stack<Character> joinStacks(Stack<Character> rightStack, Stack<Character> centerStack, Stack<Character> leftStack) {
        Stack<Character> newStack = new Stack<>();
        makeJoin(rightStack,newStack);
        makeJoin(centerStack,newStack);
        makeJoin(leftStack,newStack);
        return newStack;
    }

    /**
     * This method is responsible for passing characters from one stack to another.
     *
     * @param stack stack Host
     * @param newStack character pass-through stack
     */
    private void makeJoin(Stack<Character> stack, Stack<Character> newStack) {
        while (!stack.Empty()) {
            newStack.push(stack.pop());
        }
    }

    /**
     * This method converts an integer number into a stack.
     *
     * @param number
     * @return new Stack.
     */
    public Stack<Character> parseIntToStack(int number) {
        Stack<Character>stack = new Stack<>();
        char a;
        while (number!= 0) {
            a = (char)((number%10)+48);
            stack.push(a);
            number = number/10;
        }
        return revertStack(stack);
    }
    /**
     *This method converts a stack to an integer number.
     *
     * @param stackClass
     * @return number.
     */
    public int parseToInt(Stack<Character> stackClass) {
        stackClass = revertStack(stackClass);
        int exponent = 0 , result = 0;
        char c;
        while(!stackClass.Empty()){
            c=stackClass.pop();
            result +=(c-48)*operable.makeOperation(10,exponent);
            exponent++;
        }
        return result;
    }
}
