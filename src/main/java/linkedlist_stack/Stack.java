package linkedlist_stack;

import java.util.EmptyStackException;

public class Stack<T extends Comparable<T>> {
    private Node<T> lastNode;

    public T push(T data) {
        Node<T> newNode = new Node<>(data);
        newNode.setNext(lastNode);
        lastNode = newNode;
        return data;
    }

    public T pop() {
        if (lastNode == null) {
            throw new EmptyStackException();
        }
        T dataRemoved = lastNode.getData();
        lastNode = lastNode.getNext();
        return dataRemoved;
    }
    public boolean Empty(){
        return lastNode == null;
    }
    public String toString() {
        return String.valueOf(lastNode);
    }
}
