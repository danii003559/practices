package linkedlist_stack;

/**
 *This class contains useful methods and variables that can be used by any class.
 */
public class Utils {
    static final int FROM_ASCII_NUMBERS = 48;
    static final int UNTIL_ASCII_NUMBERS = 57;
    static final int LIMIT_NUMBER = 2147483647;
    static Stack<Character> charactersAdmitted;

    /**
     *This method return the priority level of a character into a stack.
     * @param c
     * @return index with priority level
     */
    public static int getIndexPriority(char c) {
        fillCharacterAdmitted();
        int indexReturn = -1, counter= 0;
        while(!charactersAdmitted.Empty()){
            if(c == charactersAdmitted.pop()){
                indexReturn = counter;
                break;
            }
            counter++;
        }
        return indexReturn;
    }

    /**
     * This method populates an operators stack with the priority level of the operators
     */
    private static void fillCharacterAdmitted(){
        charactersAdmitted = new Stack<>();
        charactersAdmitted.push(')');
        charactersAdmitted.push('(');
        charactersAdmitted.push('!');
        charactersAdmitted.push('^');
        charactersAdmitted.push('+');
        charactersAdmitted.push('*');
    }
    /**
     * This method checks if the character received as a parameter is a special character allowed.
     *
     * @param character
     * @return true if is a special character allowed and false if it is not.
     */
    public static boolean isACharacterSpecialAdmitted(char character) {
        boolean admitted = false;
        fillCharacterAdmitted();
        while(!charactersAdmitted.Empty()){
            if(character == charactersAdmitted.pop()){
                admitted = true;
                break;
            }
        }
        return admitted;
    }

    /**
     * This method checks if the character received as a digit.
     * @param character
     * @return true if is a digit and false if it is not.
     */
    public static  boolean isADigit(char character) {
        boolean admitted = true;
        if (character < FROM_ASCII_NUMBERS || character > UNTIL_ASCII_NUMBERS) {
            admitted = false;
        }
        return admitted;
    }
}
