package linkedlist_stack;


import linkedlist_stack.exceptions.NumberOutOfRangeException;
import linkedlist_stack.exceptions.SequenceNotAllowedException;
import linkedlist_stack.exceptions.UnsupportedCharacterException;
import linkedlist_stack.operables.Operable;
import linkedlist_stack.operables.Powers;

/**
 * This class is responsible for validating the syntax of an arithmetic operation.
 *
 * @author Daniel_Espinoza
 */
public class Validator {

    private Stack<Character> stackReview;
    private int sizeStack = 0, parenthesis = 0;
    private Operable operable;
    Parser parser;

    /**
     * This constructor method initializes the attributes used for validating arithmetic operations.
     */
    public Validator() {
        stackReview = new Stack<>();
        parser = new Parser();
        operable = new Powers();
    }

    /**
     * This method is in charge of executing the methods reviewACharacterAdmitted() and  isASequenceAdmitted().
     *
     * @param stackOperation
     * @return boolean as confirmation.
     */
    public boolean validateStack(Stack<Character> stackOperation) {
        reviewACharacterAdmitted(parser.revertStack(stackOperation));
        isASequenceAdmitted(stackReview);
        rebootProperties();
        return true;
    }


    /**
     * This method is in charge of verifying character sequences.
     *
     * @param stackSequence
     */
    private void isASequenceAdmitted(Stack<Character> stackSequence) {
        char character, characterBefore = 0;
        for (int counter = 0; !stackSequence.Empty(); counter++, characterBefore = character) {
            character = stackSequence.pop();
            if (character == '(' || character == ')' || characterBefore == '(' || characterBefore == ')') {
                controlSequenceWithBrackets(character, characterBefore);
            } else if (character == '!' || characterBefore == '!') {
                controlSequenceWithFactorial(character, characterBefore, counter);
            } else if ((counter == 0 || counter == sizeStack - 1) && Utils.isACharacterSpecialAdmitted(character)) {
                throwException("The input is invalid, it should not end in or start in any operator, as in your case '" + characterBefore + character + "'");
            } else if (counter != 0 && (Utils.isACharacterSpecialAdmitted(character) && Utils.isACharacterSpecialAdmitted(characterBefore))) {
                throwException(characterBefore, character);
            }
        }
        controlAmountParenthesis();
    }

    /**
     * This method is in charge of checking for possible syntax errors with the factorial.
     *
     * @param character
     * @param characterBefore
     * @param counter
     */
    private void controlSequenceWithFactorial(char character, char characterBefore, int counter) {
        if (characterBefore == '!' && Utils.isADigit(character)) {
            throwException(characterBefore, character);
        } else if (counter == sizeStack - 1 && character == '!' && !Utils.isADigit(characterBefore)) {
            throwException(characterBefore, character);
        }
    }

    /**
     * Trow an Exception with a message configured.
     *
     * @param a
     * @param b
     */
    private void throwException(char a, char b) {
        rebootProperties();
        throw new SequenceNotAllowedException("The Order of this Characters '" + a + b + "' in your input don't is admitted");
    }

    /**
     *Trow an Exception with a message specific.
     * @param message
     */
    private void throwException(String message) {
        rebootProperties();
        throw new SequenceNotAllowedException(message);
    }

    /**
     *This method validates if the number of parentheses is equal, otherwise it sends an exception.
     */
    private void controlAmountParenthesis() {
        if (parenthesis != 0) {
            throwException("there is an error with your parentheses inserted in your operation");
        }
    }

    /**
     *This method is in charge of checking for possible syntax errors with the Brackets.
     *
     * @param character
     * @param characterBefore
     */
    private void controlSequenceWithBrackets(char character, char characterBefore) {
        if (character == '(' && characterBefore == ')') {
            throwException("there is an error with your parentheses inserted in your operation");
        }
        if (character == ')' && characterBefore == '(') {
            throwException("Parentheses with nothing inside are not allowed '()'");
        }
        if (characterBefore == ')' && Utils.isADigit(character)) {
            throwException(characterBefore, character);
        } else if (character == '(' && Utils.isADigit(characterBefore)) {
            throwException(characterBefore, character);
        } else if (character == '(') {
            parenthesis++;
        } else if (character == ')') {
            parenthesis--;
        }
    }

    /**
     *This method checks the characters in a stack to see if they are supported characters,
     *  and also checks that no number greater than the LIMIT_NUMBER is inserted.
     *
     * @param stackOperation
     * @return verification.
     */
    private boolean reviewACharacterAdmitted(Stack<Character> stackOperation) {
        char characterStack;
        long maximNumber = 0;
        int exponent = 0;
        while (!stackOperation.Empty()) {
            characterStack = stackOperation.pop();
            stackReview.push(characterStack);
            sizeStack++;
            if (Utils.isADigit(characterStack)) {
                maximNumber = controlNumberMaxim(maximNumber, characterStack, exponent);
                exponent++;
            } else if (Utils.isACharacterSpecialAdmitted(characterStack)) {
                maximNumber = 0;
                exponent = 0;
            } else {
                rebootProperties();
                throw new UnsupportedCharacterException("Your character '" + characterStack + "' is not supported by this kind of operation");

            }
        }
        return true;
    }

    /**
     *This method verifies that no number greater than LIMIT_NUMBER is inserted.
     *
     * @param number
     * @param character
     * @param exponent
     * @return
     */
    private int controlNumberMaxim(long number, char character, int exponent) {
        number += (character - 48) * operable.makeOperation(10, exponent);
        if (number > Utils.LIMIT_NUMBER) {
            rebootProperties();
            throw new NumberOutOfRangeException("the inserted number '" + number + "' is not admitted, it exceeds the admitted limit");
        }
        return (int) number;
    }
    /**
     *This method reboot properties, for a new validation.
     */
    /**
     *
     */
    private void rebootProperties() {
        stackReview = new Stack<>();
        sizeStack = 0;
        parenthesis = 0;
    }
}
