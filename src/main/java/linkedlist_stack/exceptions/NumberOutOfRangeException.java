package linkedlist_stack.exceptions;
/**
 *This class inherits from the RunTimeException class
 * and this exception is triggered when the integer limit supported for this project is exceeded.
 *
 * @see linkedlist_stack.Utils for information about limit number.
 * @author Daniel_Espinoza
 */
public class NumberOutOfRangeException extends RuntimeException{
    public NumberOutOfRangeException(String message){super(message);}
}
