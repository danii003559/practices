package linkedlist_stack.exceptions;

/**
 *This class inherits from the RunTimeException class
 * and this exception is fired when a sequence is not valid.
 *
 * @author Daniel_Espinoza
 */
public class SequenceNotAllowedException extends RuntimeException{
    public SequenceNotAllowedException(String message){
        super(message);
    }
}
