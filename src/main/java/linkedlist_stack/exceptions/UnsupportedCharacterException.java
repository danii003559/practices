package linkedlist_stack.exceptions;

/**
 *This class inherits from the RunTimeException class
 * and this exception is fired when an invalid character is inserted into an operation.
 *
 * @see linkedlist_stack.Utils for information about supported characters.
 * @author Daniel_Espinoza
 */
public class UnsupportedCharacterException extends RuntimeException{
    public UnsupportedCharacterException(String message){
        super(message);
    }
}
