package linkedlist_stack.operables;

public class Factorial implements Operable{
    /**
     * This method calculate the factorial of number.
     *
     * @param number
     * @return factorial.
     */
    public long makeOperation(long number,long b){
        long result = 1 ;
        for(long i = number;i!=1;i--){
            result *= i;
        }
        return result;
    }
}
