package linkedlist_stack.operables;

public class Multiplication implements Operable{
    /**
     *This method multiplies the numbers a and b.
     *
     * @param a
     * @param b
     * @return result of the multiplication of a and b.
     */
    public long makeOperation(long a, long b){
        long result = a*b;
        return result;
    }
}
