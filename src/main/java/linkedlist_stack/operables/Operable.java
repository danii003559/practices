package linkedlist_stack.operables;

public interface Operable {
    long makeOperation(long a , long b);
}
