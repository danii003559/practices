package linkedlist_stack.operables;

public class Powers implements Operable{
    /**
     *This method power a base number.
     *
     * @param base
     * @param exponent
     * @return result of the powered.
     */
    public long makeOperation(long base, long exponent){
        long result = 1;
        for(long i = exponent; i>=1; i--){
            result=result*base;
        }
        return result;
    }
}
