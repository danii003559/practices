package linkedlist_stack.operables;

public class Sum implements Operable {
    /**
     *This method sum the numbers a and b.
     *
     * @param a
     * @param b
     * @return result of the sum of a and b.
     */
    public long makeOperation(long a, long b){
        return a+b;
    }
}
