package node_linkedlist;

/**
 * This class Node is a point of intersection that join a node with others.
 */
class Node {
    int data;
    Node next = null;

    /**
     * This constructor method is by default
     */
    Node() {
    }

    /**
     * this constructor method initializes with the data of the class.
     * @param data
     */
    Node(final int data) {
        this.data = data;
    }

    /**
     * this constructor method initializes with the data and the reference of the class
     * @param data
     * @param next
     */
    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    /**
     * This method appends one linked list to another.
     *
     * @param listA first node of a sequence of nodes.
     * @param listB first node of a sequence of nodes.
     * @return The head of the resulting list.
     */
    public static Node append(Node listA, Node listB) {
        Node nodeToJoin;
        if (listA == null) {
            listA = listB;
        } else {
            nodeToJoin = listA;
            while (nodeToJoin.next != null) {
                nodeToJoin = nodeToJoin.next;
            }
            nodeToJoin.next = listB;
        }
        return listA;
    }

    /**
     * This method takes a linked list and an integer index and returns the node stored at the Nth index position.
     *
     * @param n first node of a sequence of nodes.
     * @param index
     * @return  Nth index position.
     * @throws Exception this exception is returned when the index is out of range of the node length.
     */
    public static int getNth(Node n, int index) throws Exception {
        for (int i = 0; i <= index; i++) {
            if (i != 0) {
                n = n.next;
            }
        }
        if (n == null || index < 0) {
            throw new Exception();
        }
        return n.data;
    }

    public String toString() {
        return "Node = { data :" + data + " +" + next + "}";
    }
}