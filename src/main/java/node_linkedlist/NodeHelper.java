package node_linkedlist;

/**
 * This class "NodeHelper" help to test better specific methods of the class Node.
 *
 * @author DanielEspinoza
 */
public class NodeHelper {
    /**
     *This method is based on verifying that the two sequences of nodes are equal.
     *
     * @param append first node of a sequence joined.
     * @param node first node of a sequence of nodes.
     * @throws ExceptionDifferentNode this exception is returned when the two nodes don't are equals.
     */
    public static void assertEquals(Node append, Node node) throws ExceptionDifferentNode {
        boolean available = true;
        do {
            if (append.data != node.data) {
                available = false;
                break;
            }
            if ((append.next != null) == (node.next != null)) {
                append = append.next;
                node = node.next;
            } else {
                available = false;
                break;
            }
        } while (append != null);
        if (available) {
            System.out.println("WELL DONE, YOUR NODES ARE THE SAME");
        } else {
                throw new ExceptionDifferentNode("YOU'RE NODE VALUES ARE NOT THE SAME");
        }
    }

    /**
     * this method build a sequence of nodes with an arrays of the numbers.
     *
     * @param ints array of numbers.
     * @return the first node of a sequence of nodes.
     */
    public static Node build(int[] ints) {
        Node nodeCreated = null, nodeTail = null, nodeAux;
        for (int dataNode : ints) {
            nodeAux = new Node(dataNode);
            if (nodeCreated == null) {
                nodeCreated = nodeAux;
            } else {
                nodeTail.next = nodeAux;
            }
            nodeTail = nodeAux;
        }
        return nodeCreated;
    }
}

/**
 *This class inherits from the exception class, and is activated when two compared sequence nodes are not equal.
 */
class ExceptionDifferentNode extends  Exception{
    /**
     * This method builder receive a message for show to a user.
     * @param msg_Exception message about Exception.
     */
    public ExceptionDifferentNode(String msg_Exception){
            super(msg_Exception);
    }
}