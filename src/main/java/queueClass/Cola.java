package queueClass;

public class Cola {
    Node headNode;
    Node lastNode;

    public void add(int data) {
        if (headNode == null) {
            headNode = new Node(data);
            lastNode = headNode;
        }else{
            lastNode.setNext(new Node(data));
            lastNode = lastNode.getNext();
        }
    }
    public Integer remove(){
        int data;
        if(headNode!= null){
             data= headNode.getData();
            headNode = headNode.getNext();
        }else{
            return  null;
        }

        return data;
    }
    public String toString() {
        return headNode.toString();
    }
}
