package queueClass;

public class Main {
    public static void main(String[] args) {
        Cola cola = new Cola();
        cola.add(1);
        cola.add(2);
        cola.add(3);
        cola.add(4);
        System.out.println(cola);
        System.out.println(cola.remove());
        System.out.println(cola);
        cola.add(5);
        cola.add(6);
        cola.add(7);
        cola.add(8);
        System.out.println(cola);
    }


}