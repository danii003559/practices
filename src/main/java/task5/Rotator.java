package task5;

/**
 * This class is in charge of making the algorithm of rotating elements of a root work.
 *
 * @author Daniel Espinoza.
 */
public class Rotator {
    /**
     * This method returns a given array with the elements within the rotated n spaces of the array.
     *
     * @param data Array to rotate.
     * @param n  Number of times to rotate.
     * @return Array Rotated.
     */
    public Object[] rotate(Object[] data, int n) {
        Object[] arrayRotated = new Object[data.length];
        int aux, steps, indexNeg;
        steps = calculateMovement(convertPositive(n), data.length);
        for (int i = 0; i < data.length; i++) {
            if (n < 0) {
                indexNeg = convertPositive(i - data.length + 1);
                aux = searchIndex(indexNeg, steps, data.length - 1);
                arrayRotated[indexNeg] = data[aux];
            } else {
                aux = searchIndex(i, steps, data.length - 1);
                arrayRotated[aux] = data[i];
            }
        }
        return arrayRotated;
    }

    /**
     * This method simplifies the number of times to rotate.
     *
     * @param totalMovement initial number to rotary.
     * @param lengthArray Quantity of Array elements.
     * @return a Number between 0 and lengthArray.
     */
    public int calculateMovement(int totalMovement, int lengthArray) {
        int stepRotate;
        if (totalMovement >= lengthArray) {
            stepRotate = totalMovement % lengthArray;
        } else {
            stepRotate = totalMovement;
        }
        return stepRotate;
    }

    /**
     * This method is in charge of finding the index to which it will be rotated.
     *
     * @param position index of number in Array.
     * @param movement amount of steps.
     * @param lengthArray Quantity of Array elements.
     * @return index after rotation.
     */

    public int searchIndex(int position, int movement, int lengthArray) {
        int stepRotate;
        if (movement > (lengthArray - position)) {
            stepRotate = movement - (lengthArray - position) - 1;
        } else {
            stepRotate = position + movement;
        }
        return stepRotate;
    }

    /**
     * This method fulfills the function of Absolute Value.
     *
     * @return un Positive number.
     */
    private int convertPositive(int number) {
        return number < 0 ? number * -1 : number;
    }
}
