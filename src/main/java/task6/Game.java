package task6;

import task6.controller.ControllerGame;

public class Game {
    public static void main(String[] args) {
        ControllerGame controllerGame = new ControllerGame();
        controllerGame.executeGame();
    }
}
