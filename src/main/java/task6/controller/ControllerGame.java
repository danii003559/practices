package task6.controller;

import task6.model.Board;
import task6.view.Design;
import java.util.Scanner;

/**
 * This class is in charge of controlling the operation of the game.
 *
 * @author Daniel Pablo
 */
public class ControllerGame {
    private Design design;
    public static Board board;

    /**
     *This constructor method is in charge of initializing
     * the variables needed by the class to manage the flow of the game.
     */
    public ControllerGame() {
        board = new Board();
        this.design = new Design();
    }

    /**
     * This method is in charge of running the game.
     */
    public void executeGame() {
        board.generateTable();
        board.generatePlayer();
        board.generateObstacles();
        design.printWelcome();
        design.printBoard(board.getBoard());
        Attempts();
    }

    /**
     *This method is used to control the player's attempts
     */
    private void Attempts() {
        Scanner scanner = new Scanner(System.in);
        int movement;
        while (!controlArrival()) {
            design.printMenuMovements();
            movement = scanner.nextInt();
            if(!board.getPlayer().movements(movement)){
               design.printError();
            }
            design.printBoard(board.getBoard());
            board.moveObstacles();
        }
        design.printCongratulations();
    }

    /**
     *This method confirms if the player has won, is to say,if reached the finish line.
     *
     * @return
     */
    private boolean controlArrival() {
        return board.getPlayer().getPositionX() == board.getWidth()- 1 && board.getPlayer().getPositionY() == board.getHeight() - 1;
    }
}
