package task6.model;

/**
 * This class contains and generates everything that implements the game such as obstacles and players.
 *
 * @author Daniel Espinoza
 */
public class Board {
    private int width, height;
    private Obstacle obstacles[];
    private Player player;
    private String[][]board;

    /**
     * This constructor method initializes the length and height of the board.
     * And define the number of obstacles it will have
     */
    public Board() {
        this.width = 10;
        this.height = 10;
        board = new String[height][width];
        obstacles = new Obstacle[5];
    }

    /**
     *This method generates the table with [ ] for each space within it.
     */
    public void generateTable() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                board[i][j] = "[ ]";
            }
        }
        board[height - 1][width - 1] = "[T]";
    }

    /**
     *This method generates a player, and it places it in a Random location within the board.
     */
    public void generatePlayer() {
        int x = (int) (Math.random() * width);
        int y = (int) (Math.random() * height);
        player = new Player(x, y);
        board[y][x] = "[X]";
    }

    /**
     *This method generates obstacles at a random place on the board.
     *
     */
    public void generateObstacles() {
        int x,y;
        for (int i = 0; i < obstacles.length; i++) {
            x = (int) (Math.random() * (height - 1));
            y = (int) (Math.random() * (width - 1));
            if (x == player.getPositionX() && y == player.getPositionY()) {
                i--;
            } else {
                obstacles[i] = new Obstacle(x, y);
                board[y][x] = "[O]";
            }
        }
    }

    /**
     *This method changes the positions of the obstacles on the board.
     */

    public void moveObstacles() {
        int x,y;
        for (int i = 0; i < obstacles.length; i++) {
            x = (int) (Math.random() * (height - 1));
            y = (int) (Math.random() * (width - 1));
            if (x == player.getPositionX() && y == player.getPositionY()) {
                i--;
            } else {
                board[obstacles[i].getGetPositionY()][obstacles[i].getPositionX()] = "[ ]";
                obstacles[i].setPositionX(x);
                obstacles[i].setGetPositionY(y);
                board[y][x] = "[O]";
            }
        }
    }

    /**
     *This method returns an array of obstacles.
     * @return The obstacles.
     */
    public Obstacle[] getObstacles() {
        return obstacles;
    }

    /**
     *This method returns the player.
     * @return player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * This method returns the length of the board.
     * @return
     */
    public int getWidth() {
        return width;
    }

    /**
     *This method returns the Height of the board
     * @return
     */

    public int getHeight() {
        return height;
    }

    /**
     *This method returns the board.
     * @return board.
     */

    public String[][] getBoard() {
        return board;
    }
}