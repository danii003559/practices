package task6.model;

/**
 * This class contains all the information of an obstacle.
 *
 * @author Daniel Espinoza
 */
public class Obstacle {
    private int positionX;
    private int getPositionY;
    /**
     * This constructor method receives the location where the obstacle will be placed.
     *
     * @param positionX
     * @param positionY
     */
    public Obstacle(int positionX, int positionY){
        this.getPositionY = positionY;
        this.positionX = positionX;
    }

    /**
     *This method returns the X-axis position of the obstacle.
     *
     * @param positionX
     */
    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    /**
     *This method returns the Y-axis position of the obstacle.
     * @param getPositionY
     */
    public void setGetPositionY(int getPositionY) {
        this.getPositionY = getPositionY;
    }



    /**
     *This method changes the location of the obstacle on the X-axis.
     *
     * @return position in X-axis of the obstacle.
     */
    public int getPositionX() {
        return positionX;
    }

    /**
     * This method changes the location of the obstacle on the Y-axis.
     *
     * @return position in Y-axis of the obstacle.
     */
    public int getGetPositionY() {
        return getPositionY;
    }
}
