package task6.model;
import task6.controller.ControllerGame;

import javax.print.DocFlavor;

/**
 * This class is responsible for managing the player data.
 *
 * @author Daniel Espinoza
 */
public class Player {
    private int positionX;
    private int positionY;

    /**
     * This constructor method initializes the player's positions.
     *
     * @param positionX
     * @param positionY
     */
    public Player(int positionX,int positionY){
        this.positionX = positionX;
        this.positionY = positionY;
    }

    /**
     * This method returns the x-axis position of the player.
     *
     * @return
     */
    public int getPositionX() {
        return positionX;
    }

    /**
     * This method returns the y-axis position of the player.
     *
     * @return
     */
    public int getPositionY() {
        return positionY;
    }

    /**
     *This method makes the player move on the board.
     *
     * @param commando direction of Motion.
     * @return a validator
     */
    public boolean movements(int commando){
        boolean possible = true;
        int x = positionX;
        int y = positionY;
        switch (commando) {
            case 1:
                x = x + 1;
                break;
            case 2:
                x = x - 1;
                break;
            case 3:
                y = y - 1;
                break;
            case 4:
                y = y + 1;
                break;
            default:
                x=-1;y=-1;
                break;
        }
        if(controlMovement(x,y) && controlShocks(x,y)){
            ControllerGame.board.getBoard()[positionY][positionX] = "[ ]";
            ControllerGame.board.getBoard()[y][x] = "[X]";
            positionY = y;
            positionX = x;
            controlMovement(x,y);
        }else{
            possible = false;
        }
        return possible;
    }

    /**
     *this method checks that the movement that has been made does not exceed the limit of the board.
     *
     * @param x movement made in x.
     * @param y movement made in y.
     */
    public boolean controlMovement(int x, int y){
        boolean limit=false;
        if ((x>=0 && x < ControllerGame.board.getWidth()) && (y>=0 &&y < ControllerGame.board.getHeight())) {
            limit = true;
        }
        return limit;
    }

    /**
     *This method checks if the player hit an obstacle.
     *
     * @param x movement made in x.
     * @param y movement made in y.
     * @return confirmation.
     */
    public boolean controlShocks(int x,int y){
        boolean shock=true;
        for(Obstacle obstacle : ControllerGame.board.getObstacles()){
            if(obstacle.getPositionX()==x && obstacle.getGetPositionY()==y){
                shock= false;
            }
        }
        return shock;
    }
}
