package task6.view;

/**
 * This class is in charge of printing all the visual part of the program.
 *
 * @author Daniel Espinoza
 */
public class Design {
    /**
     * This class is in charge of printing all the visual part of the program.
     *
     * @param table Matrix received.
     */
    public void printBoard(String [][] table){
        for(int i = 0 ;i<10; i++){
            for(int j = 0 ; j<10;j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * This method prints a message thanking the player for winning.
     */
    public void printCongratulations(){
        System.out.println("YOU WON CONGRATULATIONS!!!!");
    }

    /**
     * This method prints a message welcoming the player.
     */
    public void printWelcome(){
        System.out.println("-----WELCOME TO MAP GAME----");
    }

    /**
     * This method prints the menu for the player to decide to make a move.
     */
    public void printMenuMovements(){
        System.out.print("1) RIGHT\t\t3)UP\n" +
                "2) LEFT \t\t4)DOWN"+
                "\nINSERT YOUR MOVEMENT : ");
    }
    public void printError(){
        System.err.println("SHOCK!!! TRY ANOTHER MOVE");
    }
}
