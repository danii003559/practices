package task7;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JUArrayList implements List<Integer> ,Iterable<Integer>{
    private int indexArray;
    private Integer [] array;
    JUIterator juIterator = new JUIterator();
    public JUArrayList(){
        array = new Integer[10];
        indexArray=0;
    }

    @Override
    public int size() {
        return indexArray;
    }

    @Override
    public boolean isEmpty() {
        return indexArray==0;
    }

    @Override
    public boolean contains(Object o) {
        boolean approval = false;
        for (Integer i: array) {
            if(o.equals(i)){
                approval = true;
                break;
            }
        }
        return approval;
    }

    @Override
    public Iterator<Integer> iterator() {
        JUIterator juIterator1 = new JUIterator();
        juIterator1.receivingArray(array);
        return juIterator1;
    }

    @Override
    public Object[] toArray() {
        Integer [] elementos = new Integer[indexArray];
        System.arraycopy(array,0,elementos,0,indexArray);
        return elementos;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        if(indexArray==array.length) {
            Integer aux[] = array;
            array = new Integer[indexArray * 2];
            System.arraycopy(aux, 0, array, 0, aux.length);
        }
        array[indexArray++] = integer;
        return false;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0 ;i<indexArray;i++) {
            if(o.equals(array[i])){
                remove(i);
                break;
            }
        }
        return contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        array = new Integer[10];
        indexArray=0;
    }

    @Override
    public Integer get(int index) {
        return array[index];
    }

    @Override
    public Integer set(int index, Integer element) {
        Integer aux = array[index];
        array[index] = element;
        return aux;
    }

    @Override
    public void add(int index, Integer element) {
        if(indexArray==array.length) {
            Integer aux[] = array;
            array = new Integer[indexArray * 2];
            System.arraycopy(aux, 0, array, 0, aux.length);
        }
        for (int i = indexArray-1; index<=i; i--) {
            array[i+1] = array[i];
            if(i==index){
                array[i] = element;
            }
        }
        indexArray++;
    }

    @Override
    public Integer remove(int index) {
        int numberIndex = array[index];
        for(int i = index ;i<indexArray-1;i++){
            array[i]=array[i+1];
        }
        indexArray--;
        return numberIndex;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }
}
