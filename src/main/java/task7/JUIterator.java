package task7;

import java.util.Iterator;

public class JUIterator implements Iterator<Integer>{
    int numberIndex = 0;
    Integer[] array ;
    public void receivingArray(Integer[] array){
        this.array = array;
    }
    @Override
    public boolean hasNext() {
        return numberIndex<array.length;
    }

    @Override
    public Integer next() {
        Integer number = array[numberIndex];
        numberIndex++;
        return number;
    }
}
