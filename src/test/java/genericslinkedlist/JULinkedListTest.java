package genericslinkedlist;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class JULinkedListTest {
    List<String> stringsList;
    List<Integer> integersList;

    @BeforeEach
    public void fillList() {
        stringsList = new JULinkedList<>("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
        integersList = new JULinkedList<>(1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    @Test
    public void sizeTest() {
        assertEquals(12, stringsList.size());
        assertEquals(9, integersList.size());
        integersList.clear();
        assertEquals(0, integersList.size());
    }

    @Test
    public void clearTest() {
        assertFalse(integersList.isEmpty());
        integersList.clear();
        assertTrue(integersList.isEmpty());

        assertFalse(stringsList.isEmpty());
        stringsList.clear();
        assertTrue(stringsList.isEmpty());
    }

    @Test
    public void isEmptyTest() {
        integersList.clear();
        assertTrue(integersList.isEmpty());
        assertEquals(0, integersList.size());

        stringsList.clear();
        assertTrue(stringsList.isEmpty());
        assertEquals(0, stringsList.size());
    }

    @Test
    public void addTest() {
        stringsList = new JULinkedList<>();
        stringsList.add("A");
        stringsList.add("B");
        stringsList.add("C");
        stringsList.add("D");
        assertArrayEquals(new String[]{"A", "B", "C", "D"}, stringsList.toArray());

        integersList = new JULinkedList<>();
        integersList.add(12);
        integersList.add(7);
        integersList.add(9);
        integersList.add(0);
        integersList.add(3);
        integersList.add(1);
        assertArrayEquals(new Integer[]{12, 7, 9, 0, 3, 1}, integersList.toArray());

        integersList = new JULinkedList<>();
        integersList.add(4);
        integersList.add(4);
        integersList.add(9);
        integersList.add(5);
        integersList.add(1);
        integersList.add(5);
        integersList.add(6);
        integersList.add(1);
        integersList.add(4);
        integersList.add(6);
        integersList.add(53);
        integersList.add(2);
        integersList.add(5);
        integersList.add(2);
        integersList.add(4);
        integersList.add(2);
        integersList.add(1);
        assertArrayEquals(new Integer[]{4, 4, 9, 5, 1, 5, 6, 1, 4, 6, 53, 2, 5, 2, 4, 2, 1}, integersList.toArray());
    }

    @Test
    public void addIndexElementTest() {
        integersList = new JULinkedList<>(1, 2, 3, 4, 5);
        integersList.add(2, 0);
        assertArrayEquals(new Integer[]{1, 2, 0, 3, 4, 5}, integersList.toArray());
        integersList.add(0, 9);
        assertArrayEquals(new Integer[]{9, 1, 2, 0, 3, 4, 5}, integersList.toArray());
        integersList.add(7, 8);
        assertArrayEquals(new Integer[]{9, 1, 2, 0, 3, 4, 5, 8}, integersList.toArray());

        stringsList = new JULinkedList<>("a", "c", "d", "r", "k", "n");
        stringsList.add(0, "A");
        assertArrayEquals(new String[]{"A", "a", "c", "d", "r", "k", "n"}, stringsList.toArray());
        stringsList.add(5, "P");
        assertArrayEquals(new String[]{"A", "a", "c", "d", "r", "P", "k", "n"}, stringsList.toArray());
        stringsList.add(8, "K");
        assertArrayEquals(new String[]{"A", "a", "c", "d", "r", "P", "k", "n", "K"}, stringsList.toArray());

    }

    @Test
    public void containTest() {
        assertTrue(stringsList.contains("A"));
        assertTrue(stringsList.contains("H"));
        assertFalse(stringsList.contains("Z"));
        assertTrue(integersList.contains(4));
        assertTrue(integersList.contains(9));
        assertFalse(integersList.contains(13));
    }

    @Test
    public void removeByObjectTest() {
        Integer number = 3;
        Assert.assertTrue(integersList.remove(number));
        Assert.assertArrayEquals(new Integer[]{1, 2, 4, 5, 6, 7, 8, 9}, integersList.toArray());
        number = 9;
        Assert.assertTrue(integersList.remove(number));
        Assert.assertArrayEquals(new Integer[]{1, 2, 4, 5, 6, 7, 8}, integersList.toArray());
        number = 1;
        Assert.assertTrue(integersList.remove(number));
        Assert.assertArrayEquals(new Integer[]{2, 4, 5, 6, 7, 8}, integersList.toArray());
        number = 12;
        Assert.assertFalse(integersList.remove(number));


        assertTrue(stringsList.remove("A"));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());
        assertTrue(stringsList.remove("F"));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());
        assertTrue(stringsList.remove("L"));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "G", "H", "I", "J", "K"}, stringsList.toArray());
        assertFalse(stringsList.remove("Z"));
    }

    @Test
    public void removeByIndexTest() {
        Integer numberExpected = 4;
        assertEquals(numberExpected, integersList.remove(3));
        assertArrayEquals(new Integer[]{1, 2, 3, 5, 6, 7, 8, 9}, integersList.toArray());
        numberExpected = 9;
        assertEquals(numberExpected, integersList.remove(7));
        assertArrayEquals(new Integer[]{1, 2, 3, 5, 6, 7, 8}, integersList.toArray());
        numberExpected = 1;
        assertEquals(numberExpected, integersList.remove(0));
        assertArrayEquals(new Integer[]{2, 3, 5, 6, 7, 8}, integersList.toArray());

        assertEquals("A", stringsList.remove(0));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());
        assertEquals("I", stringsList.remove(7));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "F", "G", "H", "J", "K", "L"}, stringsList.toArray());
        assertEquals("L", stringsList.remove(9));
        assertArrayEquals(new String[]{"B", "C", "D", "E", "F", "G", "H", "J", "K"}, stringsList.toArray());

    }

    @Test
    public void getTest() {
        assertEquals("A", stringsList.get(0));
        assertEquals("B", stringsList.get(1));
        assertEquals("L", stringsList.get(11));

        Integer numberExècted = 1;
        assertEquals(numberExècted, integersList.get(0));
        numberExècted = 2;
        assertEquals(numberExècted, integersList.get(1));
        numberExècted = 9;
        assertEquals(numberExècted, integersList.get(8));
    }

    @Test
    public void setTest() {
        assertEquals("A", stringsList.set(0, "P"));
        assertArrayEquals(new String[]{"P", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());
        assertEquals("L", stringsList.set(11, "P"));
        assertArrayEquals(new String[]{"P", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "P"}, stringsList.toArray());
        assertEquals("E", stringsList.set(4, "P"));
        assertArrayEquals(new String[]{"P", "B", "C", "D", "P", "F", "G", "H", "I", "J", "K", "P"}, stringsList.toArray());

        Integer numberExected = 1;
        assertEquals(numberExected, integersList.set(0, 0));
        assertArrayEquals(new Integer[]{0, 2, 3, 4, 5, 6, 7, 8, 9}, integersList.toArray());
        numberExected = 9;
        assertEquals(numberExected, integersList.set(8, 0));
        assertArrayEquals(new Integer[]{0, 2, 3, 4, 5, 6, 7, 8, 0}, integersList.toArray());
        numberExected = 7;
        assertEquals(numberExected, integersList.set(6, 0));
        assertArrayEquals(new Integer[]{0, 2, 3, 4, 5, 6, 0, 8, 0}, integersList.toArray());
    }

    @Test
    public void indexOfTest() {
        integersList = new JULinkedList<>(1, 2, 5, 1, 3, 2, 5, 7, 3, 2, 5, 8, 5, 2, 6);
        assertEquals(0, integersList.indexOf(1));
        assertEquals(-1, integersList.indexOf(12));
        assertEquals(4, integersList.indexOf(3));
        assertEquals(14, integersList.indexOf(6));

        stringsList = new JULinkedList<>("A", "d", "D", "v", "A", "T", "K", "r", "d", "L", "A", "a", "a", "T");
        assertEquals(11, stringsList.indexOf("a"));
        assertEquals(1, stringsList.indexOf("d"));
        assertEquals(5, stringsList.indexOf("T"));
        assertEquals(6, stringsList.indexOf("K"));
        assertEquals(-1, stringsList.indexOf("z"));
    }

    @Test
    public void lastOfTest() {
        integersList = new JULinkedList<>(1, 2, 3, 5, 2, 2, 1, 2, 2, 4, 7, 4, 6, 8, 5, 3, 5, 7, 5, 5, 2, 4, 4, 11, 3, 4, 14, 1);
        assertEquals(27, integersList.lastIndexOf(1));
        assertEquals(20, integersList.lastIndexOf(2));
        assertEquals(25, integersList.lastIndexOf(4));
        assertEquals(-1, integersList.lastIndexOf(9));

        integersList = new JULinkedList<>(5, 5, 5, 8, 1, 13, 3, 3, 1, 4, 4, 3, 5, 13);
        assertEquals(12, integersList.lastIndexOf(5));
        assertEquals(-1, integersList.lastIndexOf(10));
        assertEquals(11, integersList.lastIndexOf(3));

        stringsList = new JULinkedList<>("a", "o", "A", "K", "d", "S", "P", "a", "r", "t", "F", "K", "t", "A", "Q", "a", "Ñ", "T");
        assertEquals(15, stringsList.lastIndexOf("a"));
        assertEquals(13, stringsList.lastIndexOf("A"));
        assertEquals(-1, stringsList.lastIndexOf("k"));
        assertEquals(-1, stringsList.lastIndexOf("n"));

        stringsList = new JULinkedList<>("p", "r", "n", "K", "d", "t", "P", "a", "t", "t", "F");
        assertEquals(2, stringsList.lastIndexOf("n"));
        assertEquals(9, stringsList.lastIndexOf("t"));
        assertEquals(-1, stringsList.lastIndexOf("s"));
    }

    @Test
    public void iteratorStringTest() {
        Iterator<String> iterator = stringsList.iterator();
        Object[] arrayLinkedList = stringsList.toArray();
        int index = 0;
        while (iterator.hasNext()) {
            assertEquals(arrayLinkedList[index], iterator.next());
            index++;
        }
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void iteratorIntegerTest() {
        Iterator<Integer> iterator = integersList.iterator();
        Object[] arrayLinedList = integersList.toArray();
        int index = 0;
        while (iterator.hasNext()) {
            assertEquals(arrayLinedList[index], iterator.next());
            index++;
        }
        Assertions.assertFalse(iterator.hasNext());
    }

    @Test
    public void toArrayTest() {
        assertArrayEquals(new Object[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, integersList.toArray());
        assertArrayEquals(new Object[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());

        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, integersList.toArray(new Integer[]{}));
        assertArrayEquals(new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray(new String[]{}));

        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, integersList.toArray(new Integer[]{2, 3, 4, 2}));
        assertArrayEquals(new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray(new String[]{"D", "s", "L"}));
    }

    /**
     * El metodo puede aprobar la mayoria de los test, sin embargo , en el caso de ejecutar alguna funcion que de alguna manera
     * cambie el tamaño de la lista puede dar errores,  o por lo menos no resultados esperados.
     */
    @Test
    public void subListTest() {
        List<Integer> subList = integersList.subList(2, 6);

        assertArrayEquals(new Object[]{3, 4, 5, 6}, subList.toArray());
        assertArrayEquals(new Object[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, integersList.toArray());

        subList.set(3, 100);
        assertArrayEquals(new Object[]{3, 4, 5, 100}, subList.toArray());
        assertArrayEquals(new Object[]{1, 2, 3, 4, 5, 100, 7, 8, 9}, integersList.toArray());
        Integer numberExpected = 5;
        assertEquals(numberExpected, subList.remove(2));
        assertArrayEquals(new Object[]{3, 4, 100}, subList.toArray());

        List<String> subListString = stringsList.subList(4, 8);

        assertArrayEquals(new Object[]{"E", "F", "G", "H"}, subListString.toArray());
        assertArrayEquals(new Object[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}, stringsList.toArray());

        subListString.set(3, "o");
        assertArrayEquals(new Object[]{"E", "F", "G", "o"}, subListString.toArray());
        assertArrayEquals(new Object[]{"A", "B", "C", "D", "E", "F", "G", "o", "I", "J", "K", "L"}, stringsList.toArray());

    }
}
