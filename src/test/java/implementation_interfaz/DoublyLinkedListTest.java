package implementation_interfaz;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DoublyLinkedListTest {
    List<Integer> doublyLinkedListInteger;
    List<String> doublyLinkedListString;

    @Test
    public void isEmpty() {
        doublyLinkedListInteger = new DoublyLinkedList<>();
        assertTrue(doublyLinkedListInteger.isEmpty());
        doublyLinkedListInteger.add(33);
        assertFalse(doublyLinkedListInteger.isEmpty());
        doublyLinkedListInteger.remove(33);
        assertTrue(doublyLinkedListInteger.isEmpty());

        doublyLinkedListString = new DoublyLinkedList<>();
        assertTrue(doublyLinkedListString.isEmpty());
        doublyLinkedListString.add("Runner");
        assertFalse(doublyLinkedListString.isEmpty());
        doublyLinkedListString.remove("Runner");
        assertTrue(doublyLinkedListString.isEmpty());
    }

    @Test
    public void addIntegerTest() {
        doublyLinkedListInteger = new DoublyLinkedList<>();
        doublyLinkedListInteger.add(12);
        doublyLinkedListInteger.add(6);
        doublyLinkedListInteger.add(1);
        doublyLinkedListInteger.add(7);
        doublyLinkedListInteger.add(4);
        doublyLinkedListInteger.add(5);
        doublyLinkedListInteger.add(3);
        doublyLinkedListInteger.add(12);
        assertEquals("(8) -> { Previous : null Data : 12  Next : { Previous : 12 Data : 6  Next : { Previous : 6 Data : 1  " +
                "Next : { Previous : 1 Data : 7  Next : { Previous : 7 Data : 4  Next : { Previous : 4 Data : 5  Next : { Previous : 5 Data : 3  " +
                "Next : { Previous : 3 Data : 12  Next : null}}}}}}}}", doublyLinkedListInteger.toString());
    }

    @Test
    public void addStringTest() {
        doublyLinkedListString = new DoublyLinkedList<>();
        doublyLinkedListString.add("Dance");
        doublyLinkedListString.add("Run");
        doublyLinkedListString.add("Sun");
        doublyLinkedListString.add("eat");
        doublyLinkedListString.add("ate");
        doublyLinkedListString.add("a");
        doublyLinkedListString.add("an");
        doublyLinkedListString.add("and");
        assertEquals("(8) -> { Previous : null Data : Dance  Next : { Previous : Dance Data : Run  " +
                        "Next : { Previous : Run Data : Sun  Next : { Previous : Sun Data : eat  Next : { Previous : eat Data : ate  " +
                        "Next : { Previous : ate Data : a  Next : { Previous : a Data : an  Next : { Previous : an Data : and  Next : null}}}}}}}}"
                , doublyLinkedListString.toString());
    }

    @Test
    public void sizeTest() {
        fillInteger();
        assertEquals(8, doublyLinkedListInteger.size());
        doublyLinkedListInteger.remove(12);
        assertEquals(7, doublyLinkedListInteger.size());

        fillString();
        assertEquals(7, doublyLinkedListString.size());
        doublyLinkedListString.remove("Dance");
        assertEquals(6, doublyLinkedListString.size());
    }

    @Test
    public void removeTest() {
        fillInteger();
        assertFalse(doublyLinkedListInteger.remove(13));
        assertTrue(doublyLinkedListInteger.remove(6));
        assertTrue(doublyLinkedListInteger.remove(12));
        assertTrue(doublyLinkedListInteger.remove(12));
        System.out.println("..........................");
        assertEquals("(5) -> { Previous : null Data : 32  Next : { Previous : 32 Data : 54  Next : { Previous : 54 Data : 1  Next : " +
                "{ Previous : 1 Data : 15  Next : { Previous : 15 Data : 3  Next : null}}}}}", doublyLinkedListInteger.toString());
        fillString();
        assertFalse(doublyLinkedListString.remove("Walk"));
        assertTrue(doublyLinkedListString.remove("Run"));
        assertTrue(doublyLinkedListString.remove("and"));
        assertTrue(doublyLinkedListString.remove("Dance"));
        assertEquals("(4) -> { Previous : null Data : Sing  Next : { Previous : Sing Data : eat  Next : " +
                "{ Previous : eat Data : Jump  Next : { Previous : Jump Data : an  Next : null}}}}", doublyLinkedListString.toString());
    }

    @Test
    public void getTest() {
        fillInteger();
        assertEquals(12, doublyLinkedListInteger.get(0));
        assertEquals(6, doublyLinkedListInteger.get(7));
        assertEquals(1, doublyLinkedListInteger.get(4));
        fillString();
        assertEquals("Dance", doublyLinkedListString.get(0));
        assertEquals("eat", doublyLinkedListString.get(3));
        assertEquals("and", doublyLinkedListString.get(6));
    }

    private void fillString() {
        doublyLinkedListString = new DoublyLinkedList<>();
        doublyLinkedListString.add("Dance");
        doublyLinkedListString.add("Run");
        doublyLinkedListString.add("Sing");
        doublyLinkedListString.add("eat");
        doublyLinkedListString.add("Jump");
        doublyLinkedListString.add("an");
        doublyLinkedListString.add("and");
    }

    private void fillInteger() {
        doublyLinkedListInteger = new DoublyLinkedList<>();
        doublyLinkedListInteger.add(12);
        doublyLinkedListInteger.add(32);
        doublyLinkedListInteger.add(54);
        doublyLinkedListInteger.add(12);
        doublyLinkedListInteger.add(1);
        doublyLinkedListInteger.add(15);
        doublyLinkedListInteger.add(3);
        doublyLinkedListInteger.add(6);
    }

    @Test
    public void sceneryOne() {
        doublyLinkedListInteger = new DoublyLinkedList<>();
        assertTrue(doublyLinkedListInteger.isEmpty());
        assertTrue(doublyLinkedListInteger.add(7));
        assertTrue(doublyLinkedListInteger.add(4));
        assertTrue(doublyLinkedListInteger.add(0));
        assertTrue(doublyLinkedListInteger.add(-1));
        assertTrue(doublyLinkedListInteger.add(9));
        assertTrue(doublyLinkedListInteger.add(8));

        assertFalse(doublyLinkedListInteger.isEmpty());
        // 7 4 0 -1 9 8
        assertEquals(7, doublyLinkedListInteger.get(0));
        assertEquals(9, doublyLinkedListInteger.get(4));
        assertEquals(6, doublyLinkedListInteger.size());
        assertTrue(doublyLinkedListInteger.remove(7));
        // 4 0 -1 9 8
        assertEquals(4, doublyLinkedListInteger.get(0));
        assertEquals(8, doublyLinkedListInteger.get(4));
        assertEquals(5, doublyLinkedListInteger.size());
        assertTrue(doublyLinkedListInteger.remove(8));
        // 4 0 -1 9
        assertEquals(4, doublyLinkedListInteger.get(0));
        assertEquals(0, doublyLinkedListInteger.get(1));
        try {
            doublyLinkedListInteger.get(4);
            assertTrue(false);
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
        assertEquals(4, doublyLinkedListInteger.size());

        assertTrue(doublyLinkedListInteger.remove(0));
        // 4 -1 9
        assertEquals(4, doublyLinkedListInteger.get(0));
        assertEquals(-1, doublyLinkedListInteger.get(1));
        assertEquals(3, doublyLinkedListInteger.size());
        // 4 -1 9
        assertTrue(doublyLinkedListInteger.add(5));
        // 4 -1 9 5
        assertEquals(4, doublyLinkedListInteger.get(0));
        assertEquals(5, doublyLinkedListInteger.get(3));
        assertEquals(4, doublyLinkedListInteger.size());

        assertTrue(doublyLinkedListInteger.remove(4));
        assertTrue(doublyLinkedListInteger.remove(-1));
        assertTrue(doublyLinkedListInteger.remove(5));
        assertEquals(9, doublyLinkedListInteger.get(0));
        // 9
        assertTrue(doublyLinkedListInteger.add(1));
        assertTrue(doublyLinkedListInteger.add(2));
        assertTrue(doublyLinkedListInteger.add(4));
        assertEquals(4, doublyLinkedListInteger.size());
        // 9 1 2 4

        assertEquals(9, doublyLinkedListInteger.get(0));
        assertEquals(1, doublyLinkedListInteger.get(1));
        assertEquals(2, doublyLinkedListInteger.get(2));
        assertEquals(4, doublyLinkedListInteger.get(3));

        assertTrue(doublyLinkedListInteger.remove(9));
        assertTrue(doublyLinkedListInteger.remove(1));
        assertTrue(doublyLinkedListInteger.remove(2));
        assertEquals(1, doublyLinkedListInteger.size());
        // 4
        assertTrue(doublyLinkedListInteger.remove(4));
        // null
        assertTrue(doublyLinkedListInteger.isEmpty());
        assertEquals(0, doublyLinkedListInteger.size());
    }

    @Test
    public void sceneryTwo() {
        doublyLinkedListString = new DoublyLinkedList<>();
        assertTrue(doublyLinkedListString.isEmpty());
        assertTrue(doublyLinkedListString.add("A"));
        assertTrue(doublyLinkedListString.add("B"));
        assertTrue(doublyLinkedListString.add("C"));
        assertTrue(doublyLinkedListString.add("D"));
        assertTrue(doublyLinkedListString.add("E"));
        assertTrue(doublyLinkedListString.add("F"));
        assertTrue(doublyLinkedListString.add("G"));

        assertEquals(7, doublyLinkedListString.size());
        assertFalse(doublyLinkedListString.isEmpty());
        assertEquals("A", doublyLinkedListString.get(0));
        assertEquals("G", doublyLinkedListString.get(6));
        // A B C D E F G

        assertTrue(doublyLinkedListString.remove("A"));
        assertTrue(doublyLinkedListString.remove("G"));
        assertTrue(doublyLinkedListString.remove("D"));
        assertTrue(doublyLinkedListString.remove("E"));
        // B C F

        assertEquals(3, doublyLinkedListString.size());
        assertEquals("B", doublyLinkedListString.get(0));
        assertEquals("C", doublyLinkedListString.get(1));
        assertEquals("F", doublyLinkedListString.get(2));
        try {
            assertEquals("", doublyLinkedListString.get(3));
            assertTrue(false);
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }

        assertTrue(doublyLinkedListString.add("Z"));
        assertTrue(doublyLinkedListString.remove("B"));
        assertFalse(doublyLinkedListString.remove("B"));

        assertTrue(doublyLinkedListString.add("K"));
        assertTrue(doublyLinkedListString.remove("F"));

        assertEquals("C", doublyLinkedListString.get(0));
        assertEquals("Z", doublyLinkedListString.get(1));
        assertEquals("K", doublyLinkedListString.get(2));
        //C Z K

        assertTrue(doublyLinkedListString.remove("C"));
        assertTrue(doublyLinkedListString.remove("Z"));
        assertTrue(doublyLinkedListString.remove("K"));

        assertTrue(doublyLinkedListString.isEmpty());

        assertTrue(doublyLinkedListString.add("M"));
        assertTrue(doublyLinkedListString.add(" "));
        assertTrue(doublyLinkedListString.add(""));
        // M, " ", ""
        assertEquals("M", doublyLinkedListString.get(0));
        assertEquals(" ", doublyLinkedListString.get(1));
        try {
            assertEquals(" ", doublyLinkedListString.get(-1));
            assertTrue(false);
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
        assertEquals(3, doublyLinkedListString.size());
        assertTrue(doublyLinkedListString.remove(" "));
        // M ,""
        assertEquals("M", doublyLinkedListString.get(0));
        assertEquals("", doublyLinkedListString.get(1));
    }
}
