package implementation_interfaz;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class xChangeMethodTest {
    DoublyLinkedList<Integer> doublyLinkedListInteger;
    DoublyLinkedList<String> doublyLinkedListString;
    @Test
    public void longChangeTest(){
        doublyLinkedListString = new DoublyLinkedList<>();
        doublyLinkedListString.add("A");
        doublyLinkedListString.add("B");
        doublyLinkedListString.add("C");
        doublyLinkedListString.add("D");
        doublyLinkedListString.add("E");
        doublyLinkedListString.add("F");
        // A B C D E F
        doublyLinkedListString.xchange(0,5);
        // F B C D E A
        assertEquals("F",doublyLinkedListString.get(0));
        assertEquals("B",doublyLinkedListString.get(1));
        assertEquals("C",doublyLinkedListString.get(2));
        assertEquals("D",doublyLinkedListString.get(3));
        assertEquals("E",doublyLinkedListString.get(4));
        assertEquals("A",doublyLinkedListString.get(5));

        doublyLinkedListString.xchange(0,2);
        //// C B F D E A
        assertEquals("C",doublyLinkedListString.get(0));
        assertEquals("B",doublyLinkedListString.get(1));
        assertEquals("F",doublyLinkedListString.get(2));
        assertEquals("D",doublyLinkedListString.get(3));
        assertEquals("E",doublyLinkedListString.get(4));
        assertEquals("A",doublyLinkedListString.get(5));

        doublyLinkedListString.xchange(2,4);
        //// C B E D F A
        assertEquals("C",doublyLinkedListString.get(0));
        assertEquals("B",doublyLinkedListString.get(1));
        assertEquals("E",doublyLinkedListString.get(2));
        assertEquals("D",doublyLinkedListString.get(3));
        assertEquals("F",doublyLinkedListString.get(4));
        assertEquals("A",doublyLinkedListString.get(5));

        doublyLinkedListString.xchange(1,5);
        //// C A E D F B
        assertEquals("C",doublyLinkedListString.get(0));
        assertEquals("A",doublyLinkedListString.get(1));
        assertEquals("E",doublyLinkedListString.get(2));
        assertEquals("D",doublyLinkedListString.get(3));
        assertEquals("F",doublyLinkedListString.get(4));
        assertEquals("B",doublyLinkedListString.get(5));
    }
    @Test
    public void changeToNextTest(){
        doublyLinkedListInteger = new DoublyLinkedList<>();
        doublyLinkedListInteger.add(1);
        doublyLinkedListInteger.add(2);
        doublyLinkedListInteger.add(3);
        doublyLinkedListInteger.add(4);
        doublyLinkedListInteger.add(5);
        //1 2 3 4 5
        doublyLinkedListInteger.xchange(2,3);
        //1 2 4 3 5
        assertEquals(1,doublyLinkedListInteger.get(0));
        assertEquals(2,doublyLinkedListInteger.get(1));
        assertEquals(4,doublyLinkedListInteger.get(2));
        assertEquals(3,doublyLinkedListInteger.get(3));
        assertEquals(5,doublyLinkedListInteger.get(4));

        doublyLinkedListInteger.xchange(0,1);
        //2 1 4 3 5
        assertEquals(2,doublyLinkedListInteger.get(0));
        assertEquals(1,doublyLinkedListInteger.get(1));
        assertEquals(4,doublyLinkedListInteger.get(2));
        assertEquals(3,doublyLinkedListInteger.get(3));
        assertEquals(5,doublyLinkedListInteger.get(4));

        doublyLinkedListInteger.xchange(3,4);
        //2 1 4 5 3
        assertEquals(2,doublyLinkedListInteger.get(0));
        assertEquals(1,doublyLinkedListInteger.get(1));
        assertEquals(4,doublyLinkedListInteger.get(2));
        assertEquals(5,doublyLinkedListInteger.get(3));
        assertEquals(3,doublyLinkedListInteger.get(4));
    }

    @Test
    public void xChangeTest(){
        doublyLinkedListString = new DoublyLinkedList<>();
        doublyLinkedListString.add("SOME");
        doublyLinkedListString.add("PLAY");
        doublyLinkedListString.add("HAVE");
        doublyLinkedListString.add("EAT");
        doublyLinkedListString.add("RUN");
        doublyLinkedListString.add("CAT");
        doublyLinkedListString.add("CHICKEN");
        doublyLinkedListString.add("SUN");
        doublyLinkedListString.xchange(3,5);
        assertEquals("(8) -> { Previous : null Data : SOME  Next : { Previous : SOME Data : PLAY  Next : { Previous : PLAY Data : HAVE  Next :" +
                        " { Previous : HAVE Data : CAT  Next : { Previous : CAT Data : RUN  Next : { Previous : RUN Data : EAT  Next : { Previous : EAT Data : CHICKEN  Next :" +
                        " { Previous : CHICKEN Data : SUN  Next : null}}}}}}}}"
                ,doublyLinkedListString.toString());
        doublyLinkedListString.xchange(0,7);
        assertEquals("(8) -> { Previous : null Data : SUN  Next : { Previous : SUN Data : PLAY  Next : { Previous : PLAY Data : HAVE  Next : { Previous : HAVE Data : CAT  Next :" +
                " { Previous : CAT Data : RUN  Next : { Previous : RUN Data : EAT  Next : { Previous : EAT Data : CHICKEN  Next : { Previous : CHICKEN Data : SOME  Next : null}}}}}}}}"
                ,doublyLinkedListString.toString());
    }
}
