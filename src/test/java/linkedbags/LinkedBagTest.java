package linkedbags;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LinkedBagTest {
    Bag<Integer> linkedBagIntegers;
    Bag<String> linkedBagStrings;

    //SELECTION SORT TEST
    @Test
    public void selectionSortTest(){
        linkedBagIntegers = new LinkedBag<>();
        linkedBagIntegers.add(12);
        linkedBagIntegers.add(3);
        linkedBagIntegers.add(-5);
        linkedBagIntegers.add(3);
        linkedBagIntegers.add(-1);
        linkedBagIntegers.add(2);
        linkedBagIntegers.add(2);
        linkedBagIntegers.add(9);
        linkedBagIntegers.add(7);
        linkedBagIntegers.add(0);
        linkedBagIntegers.selectionSort();
        assertEquals("(10)root={data=12, sig->{data=9, sig->{data=7, sig->{data=3, sig->{data=3, sig->{data=2, sig->{data=2, sig->{data=0, sig->{data=-1, sig->{data=-5, sig->null}}}}}}}}}}"
                ,linkedBagIntegers.toString());
    }
    @Test
    public void selectionSortTwoTest(){
        linkedBagStrings = new LinkedBag<>();
        linkedBagStrings.add("z");
        linkedBagStrings.add("b");
        linkedBagStrings.add("A");
        linkedBagStrings.add("k");
        linkedBagStrings.add("A");
        linkedBagStrings.add("");
        linkedBagStrings.add("a");
        linkedBagStrings.add("A");
        linkedBagStrings.add("l");
        linkedBagStrings.add("L");
        linkedBagStrings.selectionSort();
        assertEquals("(10)root={data=z, sig->{data=l, sig->{data=k, sig->{data=b, sig->{data=a, sig->{data=L, sig->{data=A, sig->{data=A, sig->{data=A, sig->{data=, sig->null}}}}}}}}}}",
                linkedBagStrings.toString());
    }
    @Test
    public void selectionSortThreeTest(){
        linkedBagStrings = new LinkedBag<>();
        linkedBagStrings.add("Hi");
        linkedBagStrings.add("World");
        linkedBagStrings.add("eat");
        linkedBagStrings.add("jump");
        linkedBagStrings.add("Jump");
        linkedBagStrings.add("start");
        linkedBagStrings.add("make");
        linkedBagStrings.add("Jake");
        linkedBagStrings.add("tomato");
        linkedBagStrings.add("potato");
        linkedBagStrings.selectionSort();
        assertEquals("(10)root={data=tomato, sig->{data=start, sig->{data=potato, sig->{data=make, sig->{data=jump, sig->{data=eat, sig->{data=World, sig->{data=Jump, sig->{data=Jake, sig->{data=Hi, sig->null}}}}}}}}}}",
                linkedBagStrings.toString());
    }

    //BUBBLE SORT TEST
    @Test
    public void bobbleSortTest(){
        linkedBagIntegers = new LinkedBag<>();
        linkedBagIntegers.add(17);
        linkedBagIntegers.add(4);
        linkedBagIntegers.add(-5);
        linkedBagIntegers.add(1);
        linkedBagIntegers.add(-1);
        linkedBagIntegers.add(1);
        linkedBagIntegers.add(2);
        linkedBagIntegers.add(9);
        linkedBagIntegers.add(7);
        linkedBagIntegers.add(0);
        linkedBagIntegers.add(17);
        linkedBagIntegers.bubbleSort();
        assertEquals("(11)root={data=17, sig->{data=17, sig->{data=9, sig->{data=7, sig->{data=4, sig->{data=2, sig->{data=1, sig->{data=1, sig->{data=0, sig->{data=-1, sig->{data=-5, sig->null}}}}}}}}}}}"
                ,linkedBagIntegers.toString());
    }

    @Test
    public void bubbleSortTwoTest(){
        linkedBagStrings = new LinkedBag<>();
        linkedBagStrings.add("N");
        linkedBagStrings.add("r");
        linkedBagStrings.add("A");
        linkedBagStrings.add("T");
        linkedBagStrings.add("A");
        linkedBagStrings.add("s");
        linkedBagStrings.add("s");
        linkedBagStrings.add("Z");
        linkedBagStrings.add("M");
        linkedBagStrings.add("L");
        linkedBagStrings.add("");
        linkedBagStrings.add("s");
        linkedBagStrings.bubbleSort();
        assertEquals("(12)root={data=s, sig->{data=s, sig->{data=s, sig->{data=r, sig->{data=Z, sig->{data=T, sig->{data=N, sig->{data=M, sig->{data=L, sig->{data=A, sig->{data=A, sig->{data=, sig->null}}}}}}}}}}}}"
               ,linkedBagStrings.toString());
    }
    @Test
    public void bubbleSortThreeTest(){
        linkedBagStrings = new LinkedBag<>();
        linkedBagStrings.add("RUN");
        linkedBagStrings.add("run");
        linkedBagStrings.add("DANCE");
        linkedBagStrings.add("ride");
        linkedBagStrings.add("ride");
        linkedBagStrings.add("Walk");
        linkedBagStrings.add("eat");
        linkedBagStrings.add("drink");
        linkedBagStrings.add("break");
        linkedBagStrings.add("jump");
        linkedBagStrings.bubbleSort();
        assertEquals("(10)root={data=run, sig->{data=ride, sig->{data=ride, sig->{data=jump, sig->{data=eat, sig->{data=drink, sig->{data=break, sig->{data=Walk, sig->{data=RUN, sig->{data=DANCE, sig->null}}}}}}}}}}",linkedBagStrings.toString());
    }
    // MERGE SORT
    @Test
    public void mergeSortTest(){
        linkedBagIntegers = new LinkedBag<>();
        linkedBagIntegers.add(8);
        linkedBagIntegers.add(1);
        linkedBagIntegers.add(2);
        linkedBagIntegers.add(-5);
        linkedBagIntegers.add(0);
        linkedBagIntegers.add(9);
        linkedBagIntegers.add(12);
        linkedBagIntegers.add(9);
        linkedBagIntegers.add(8);
        linkedBagIntegers.add(-12);
        linkedBagIntegers.mergeSort();
        assertEquals("(10)root={data=12, sig->{data=9, sig->{data=9, sig->{data=8, sig->{data=8, sig->{data=2, sig->{data=1, sig->{data=0, sig->{data=-5, sig->{data=-12, sig->null}}}}}}}}}}",linkedBagIntegers.toString());
    }
    @Test
    public void mergeSortTwoTest(){
        linkedBagStrings = new LinkedBag<>();
        linkedBagStrings.add("N");
        linkedBagStrings.add("r");
        linkedBagStrings.add("A");
        linkedBagStrings.add("T");
        linkedBagStrings.add("A");
        linkedBagStrings.add("s");
        linkedBagStrings.add("s");
        linkedBagStrings.add("Z");
        linkedBagStrings.add("M");
        linkedBagStrings.add("L");
        linkedBagStrings.add("");
        linkedBagStrings.add("s");
        linkedBagStrings.mergeSort();
        assertEquals("(12)root={data=s, sig->{data=s, sig->{data=s, sig->{data=r, sig->{data=Z, sig->{data=T, sig->{data=N, sig->{data=M, sig->{data=L, sig->{data=A, sig->{data=A, sig->{data=, sig->null}}}}}}}}}}}}"
                ,linkedBagStrings.toString());
    }
}
