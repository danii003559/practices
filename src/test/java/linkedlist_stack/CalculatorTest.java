package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void initDependencies() {
        calculator = new Calculator();
    }

    @Test
    public void simpleOperationsTest() {
        assertEquals(10, calculator.calculate("2^3+2"));
        assertEquals(48, calculator.calculate("4^2*3"));
        assertEquals(26, calculator.calculate("18+8"));
        assertEquals(36, calculator.calculate("3!^2"));
        assertEquals(130, calculator.calculate("5! + 10"));
        assertEquals(1468, calculator.calculate("3^3!+5*2"));
        assertEquals(780300, calculator.calculate("27*1+4!*34^2"));
        assertEquals(94091762, calculator.calculate("19+19*1*19^(3+2)"));
    }

    @Test
    public void intermediateOperationsTest() {
        assertEquals(6, calculator.calculate("(1+2)+3"));
        assertEquals(20, calculator.calculate("2*(2*5)"));
        assertEquals(10, calculator.calculate("(2^3)+2"));
        assertEquals(32, calculator.calculate("2 ^ ( 3 + 2 )"));
        assertEquals(15, calculator.calculate("(1+2)*5"));
        assertEquals(11, calculator.calculate("1 + (2 * 5 )"));
        assertEquals(120, calculator.calculate("(3 + 2)!"));
        assertEquals(36, calculator.calculate("(3!)^2 "));
        assertEquals(739, calculator.calculate("3^3!+(5*2)"));
        assertEquals(4096, calculator.calculate("4^(2*3)"));
        assertEquals(279468592, calculator.calculate("8!+19*(19^3+12)+(19*3)"));

    }

    @Test
    public void hardOperationsTest() {
        try {
            assertEquals(58610276054491l, calculator.calculate("((17))+(12)*(7*19^8)*17"));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit",e.toString());
        }
        assertEquals(921385369, calculator.calculate("1+((9*((18*15)+12))*(12^2+(8!*9)+12))"));
        assertEquals(119694300, calculator.calculate("( 1 2 * ( 7 8 )+( 17 ) * 1 9 +( 18 ) ^ ( 2 ^ 2 ) )"));
        try {
            assertEquals(31250515931l, calculator.calculate("( 1 7 * ( 1 7 +  1 8 ) ^ 6 + 1 8 )"));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit",e.toString());
        }
        try {
            assertEquals(121645100409225447l, calculator.calculate("18+19!+(9!+17)+(15*121)+13^2^2^1+((1*12)+2^(9^(2))+(12*12))"));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit",e.toString());
        }
    }

}
