package linkedlist_stack;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactorialTest {
    private Calculator calculator;
    @BeforeEach
    public void initDependencies(){
        calculator = new Calculator();
    }
    @Test
    public void simpleOperationsTest() {
        assertEquals(2, calculator.calculate("2!"));
        assertEquals(479001600, calculator.calculate("12!"));
        assertEquals(5040, calculator.calculate("7!"));
        assertEquals(26, calculator.calculate("4!+2!"));
        assertEquals(10080, calculator.calculate("7!*2!"));
        assertEquals(45384, calculator.calculate("8!+4!+7!"));
        assertEquals(479003280, calculator.calculate("5!+6!+12!+5!+6!"));
        assertEquals(46233, calculator.calculate("8!+7!+6!+5!+4!+3!+2!+1!"));
    }
}
