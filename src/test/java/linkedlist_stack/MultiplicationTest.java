package linkedlist_stack;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplicationTest {
    private Calculator calculator;
    @BeforeEach
    public void initDependencies(){
        calculator = new Calculator();
    }
    @Test
    public void simpleOperationsTest() {
        assertEquals(16, calculator.calculate("2*8"));
        assertEquals(36, calculator.calculate("4*3*3"));
        assertEquals(260, calculator.calculate("4*13*1*5"));
        assertEquals(2606976, calculator.calculate("4*31*1*73*12*24"));
        assertEquals(843897600, calculator.calculate("1*56*84*5*5*6*52*23"));
    }
}
