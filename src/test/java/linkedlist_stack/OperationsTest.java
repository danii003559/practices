package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationsTest {
    Operations operations = new Operations();

    @Test
    public void operationsSumaTest() {
        assertEquals(12, operations.operate(6, '+', 6));
        assertEquals(13, operations.operate(1, '+', 12));
        assertEquals(330, operations.operate(10, '+', 320));
        assertEquals(2700, operations.operate(1200, '+', 1500));
        try {
            assertEquals(2147483648l, operations.operate(2147483647, '+', 1));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
    }

    @Test
    public void operationsMultiplicationTest() {
        assertEquals(2, operations.operate(2, '*', 1));
        assertEquals(72, operations.operate(6, '*', 12));
        try {
            assertEquals(5320106085l, operations.operate(5, '*', 760015155));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
        try {
            assertEquals(13664552720l, operations.operate(7280, '*', 1876999));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
    }

    @Test
    public void operationsPowersTest() {
        assertEquals(4, operations.operate(2, '^', 2));
        assertEquals(244140625, operations.operate(5, '^', 12));
        try {
            assertEquals(4294967296l, operations.operate(2, '^', 32));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
        try {
            assertEquals(30517578125l, operations.operate(5, '^', 15));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
    }

    @Test
    public void operationsFactorialTest() {
        assertEquals(2, operations.operate(2, '!', 0));
        assertEquals(720, operations.operate(6, '!', 0));
        assertEquals(479001600, operations.operate(12, '!', 0));

        try {
            assertEquals(1307674368000l, operations.operate(15, '!', 0));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
    }
}
