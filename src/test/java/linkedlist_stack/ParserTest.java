package linkedlist_stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTest {
    Parser parser = new Parser();
    @Test
    public void parseToStackTest(){
        Stack<String> stackTest = parser.parseToStack("2+1");
        assertEquals("{Node -> Data : 2 next : {Node -> Data : + next : {Node -> Data : 1 next : null } } }",
                stackTest.toString());

    }
    @Test
    public void parseToStackTwoTest(){
        Stack<String> stackTest = parser.parseToStack("21+11");
        assertEquals("{Node -> Data : 2 next : {Node -> Data : 1 next : {Node -> Data : + next : {Node -> Data : 1 next : {Node -> Data : 1 next : null } } } } }",
                stackTest.toString());

    }

    @Test
    public void revertStackTest(){
        Stack<Character> stack  = new Stack<>();
        stack.push('a');
        stack.push('b');
        stack.push('c');
        stack.push('d');
        stack.push('e');
        assertEquals("{Node -> Data : a next : {Node -> Data : b next : {Node -> Data : c next : {Node -> Data : d next : {Node -> Data : e next : null } } } } }",
                parser.revertStack(stack).toString());
    }
    @Test
    public void revertStackTwoTest(){
        Stack<Character> stack  = new Stack<>();
        stack.push('1');
        stack.push('2');
        stack.push('3');
        stack.push('4');
        stack.push('5');
        assertEquals("{Node -> Data : 1 next : {Node -> Data : 2 next : {Node -> Data : 3 next : {Node -> Data : 4 next : {Node -> Data : 5 next : null } } } } }",
                parser.revertStack(stack).toString());
    }

    @Test
    public void joinStacksTestOne(){
        Stack<Character> stackOne = parser.parseToStack("ab");
        Stack<Character> stackTwo = parser.parseToStack("c");
        Stack<Character> stackThree = parser.parseToStack("de");
        assertEquals("{Node -> Data : e next : {Node -> Data : d next : {Node -> Data : c next : {Node -> Data : b next : {Node -> Data : a next : null } } } } }",
                parser.joinStacks(stackOne,stackTwo,stackThree).toString());
    }
    @Test
    public void joinStacksTestTwo() {
        Stack<Character> stackOne = parser.parseToStack("43");
        Stack<Character> stackTwo = parser.parseToStack("21");
        Stack<Character> stackThree = parser.parseToStack("");
        assertEquals("{Node -> Data : 1 next : {Node -> Data : 2 next : {Node -> Data : 3 next : {Node -> Data : 4 next : null } } } }",
                parser.joinStacks(stackOne, stackTwo, stackThree).toString());
    }
    @Test
    public void parserIntoToStack(){

        Stack<Character> stack = parser.parseIntToStack(123);
        assertEquals("{Node -> Data : 3 next : {Node -> Data : 2 next : {Node -> Data : 1 next : null } } }"
                        ,stack.toString());

        stack = parser.parseIntToStack(1000);
        assertEquals("{Node -> Data : 0 next : {Node -> Data : 0 next : {Node -> Data : 0 next : {Node -> Data : 1 next : null } } } }"
                        ,stack.toString());

        stack = parser.parseIntToStack(21);
        assertEquals("{Node -> Data : 1 next : {Node -> Data : 2 next : null } }"
                        ,stack.toString());
    }

    @Test
    public void parseStackToIntTest(){
        Stack stack = parser.parseToStack("17");
        assertEquals(17,parser.parseToInt(stack));

        stack = parser.parseToStack("231");
        assertEquals(231,parser.parseToInt(stack));

         stack = parser.parseToStack("17621");
        assertEquals(17621,parser.parseToInt(stack));

        stack = parser.parseToStack("0");
        assertEquals(0,parser.parseToInt(stack));
    }
}
