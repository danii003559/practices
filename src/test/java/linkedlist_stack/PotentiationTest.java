package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PotentiationTest {
    private Calculator calculator;

    @BeforeEach
    public void initDependencies() {
        calculator = new Calculator();
    }

    @Test
    public void simpleOperationsTest() {
        assertEquals(2, calculator.calculate("2^1"));
        assertEquals(625, calculator.calculate("5^4"));
        try {
            assertEquals(2251799813685248l, calculator.calculate("8^17"));
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
        assertEquals(16777216, calculator.calculate("8^2^3"));
        try {
           calculator.calculate("3^2^4^2");
        } catch (NumberOutOfRangeException e) {
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit", e.toString());
        }
    }
}
