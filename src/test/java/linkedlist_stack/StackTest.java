package linkedlist_stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.EmptyStackException;


public class StackTest {
    Stack<Integer> stackIntegers;
    Stack<String> stackString;

    @BeforeEach
    public void fillStack() {
        stackIntegers = new Stack<>();
        stackIntegers.push(1);
        stackIntegers.push(2);
        stackIntegers.push(3);
        stackIntegers.push(4);
        stackIntegers.push(5);
        stackIntegers.push(6);

        stackString = new Stack<>();
        stackString.push("A");
        stackString.push("B");
        stackString.push("C");
        stackString.push("D");
        stackString.push("E");
        stackString.push("F");
    }

    @Test
    public void pushTest() {
        // Test with Integers data
        stackIntegers = new Stack<>();
        assertEquals(12, stackIntegers.push(12));
        assertEquals(15, stackIntegers.push(15));
        assertEquals(18, stackIntegers.push(18));
        assertEquals(21, stackIntegers.push(21));
        assertEquals("{Node -> Data : 21 next : {Node -> Data : 18 next : {Node -> Data : 15 next : {Node -> Data : 12 next : null } } } }", stackIntegers.toString());
        // Test with Strings data
        stackString = new Stack<>();
        assertEquals("A", stackString.push("A"));
        assertEquals("B", stackString.push("B"));
        assertEquals("C", stackString.push("C"));
        assertEquals("D", stackString.push("D"));
        assertEquals("{Node -> Data : D next : {Node -> Data : C next : {Node -> Data : B next : {Node -> Data : A next : null } } } }", stackString.toString());
    }

    @Test
    public void popTest() {
        // Test with Integers data
        //1 , 2 , 3 , 4 , 5 , 6
        assertEquals(6, stackIntegers.pop());
        //1 , 2 , 3 , 4 , 5
        assertEquals("{Node -> Data : 5 next : {Node -> Data : 4 next : {Node -> Data : 3 next : {Node -> Data : 2 next : {Node -> Data : 1 next : null } } } } }", stackIntegers.toString());
        assertEquals(5, stackIntegers.pop());
        assertEquals(4, stackIntegers.pop());
        assertEquals(3, stackIntegers.pop());
        //1 , 2
        assertEquals("{Node -> Data : 2 next : {Node -> Data : 1 next : null } }", stackIntegers.toString());
        // Test with Strings data
        // A , B , C , D, E , F
        assertEquals("F", stackString.pop());
        // A , B , C , D, E
        assertEquals("{Node -> Data : E next : {Node -> Data : D next : {Node -> Data : C next : {Node -> Data : B next : {Node -> Data : A next : null } } } } }", stackString.toString());
        assertEquals("E", stackString.pop());
        assertEquals("D", stackString.pop());
        assertEquals("C", stackString.pop());
        // A , B
        assertEquals("{Node -> Data : B next : {Node -> Data : A next : null } }", stackString.toString());
    }

    @Test
    public void scenery() {
        stackIntegers = new Stack<>();
        // 1 , 2 , 3 , 4
        assertEquals(1, stackIntegers.push(1));
        assertEquals(2, stackIntegers.push(2));
        assertEquals(3, stackIntegers.push(3));
        assertEquals(4, stackIntegers.push(4));

        assertEquals("{Node -> Data : 4 next : {Node -> Data : 3 next : {Node -> Data : 2 next : {Node -> Data : 1 next : null } } } }", stackIntegers.toString());

        assertEquals(4, stackIntegers.pop());
        // 1 , 2 , 3
        assertEquals("{Node -> Data : 3 next : {Node -> Data : 2 next : {Node -> Data : 1 next : null } } }", stackIntegers.toString());

        assertEquals(3, stackIntegers.pop());
        // 1 , 2
        assertEquals("{Node -> Data : 2 next : {Node -> Data : 1 next : null } }", stackIntegers.toString());

        assertEquals(8, stackIntegers.push(8));
        // 1 , 2 , 8
        assertEquals("{Node -> Data : 8 next : {Node -> Data : 2 next : {Node -> Data : 1 next : null } } }", stackIntegers.toString());
        assertEquals(8, stackIntegers.pop());
        assertEquals(2, stackIntegers.pop());
        assertEquals(1, stackIntegers.pop());
        assertEquals("null", stackIntegers.toString());
        try {
            stackIntegers.pop();
        } catch (EmptyStackException e) {
            assertTrue(true);
        }

        assertEquals(1, stackIntegers.push(1));
        assertEquals("{Node -> Data : 1 next : null }", stackIntegers.toString());
    }
}

