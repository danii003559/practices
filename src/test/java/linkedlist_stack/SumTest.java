package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import linkedlist_stack.exceptions.UnsupportedCharacterException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class SumTest {
    private Calculator calculator;
    @BeforeEach
    public void initDependencies(){
        calculator = new Calculator();
    }
    @Test
    public void simpleOperationsTest() {
        assertEquals(5, calculator.calculate("2+3"));
        assertEquals(9, calculator.calculate("4+2+3"));
        assertEquals(13, calculator.calculate("4+3+1+5"));
        assertEquals(49, calculator.calculate("4+4+1+5+11+24"));
        assertEquals(2735, calculator.calculate("7+2+(552+(6+3)+2113)+52"));
        assertEquals(1690, calculator.calculate("736+875+23+56"));
        try{
            assertEquals(2147483648l,calculator.calculate("2147483647+1"));
        }catch(NumberOutOfRangeException e){
            assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: The operation exceeded the allowed integer number limit",e.toString());
        }
    }
}
