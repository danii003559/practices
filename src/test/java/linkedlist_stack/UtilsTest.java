package linkedlist_stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtilsTest {

    @Test
    public void getIndexPriorityTest(){
        assertEquals(0,Utils.getIndexPriority('*'));
        assertEquals(1,Utils.getIndexPriority('+'));
        assertEquals(2,Utils.getIndexPriority('^'));
        assertEquals(3,Utils.getIndexPriority('!'));
        assertEquals(4,Utils.getIndexPriority('('));
        assertEquals(5,Utils.getIndexPriority(')'));
        assertEquals(-1,Utils.getIndexPriority('a'));
        assertEquals(-1,Utils.getIndexPriority('1'));
    }

    @Test
    public void isACharacterSpecialAdmittedTest(){
        assertEquals(true,Utils.isACharacterSpecialAdmitted('*'));
        assertEquals(true,Utils.isACharacterSpecialAdmitted('+'));
        assertEquals(true,Utils.isACharacterSpecialAdmitted('^'));
        assertEquals(true,Utils.isACharacterSpecialAdmitted('!'));
        assertEquals(true,Utils.isACharacterSpecialAdmitted('('));
        assertEquals(true,Utils.isACharacterSpecialAdmitted(')'));
        assertEquals(false,Utils.isACharacterSpecialAdmitted('-'));
        assertEquals(false,Utils.isACharacterSpecialAdmitted('/'));
        assertEquals(false,Utils.isACharacterSpecialAdmitted('?'));
        assertEquals(false,Utils.isACharacterSpecialAdmitted('k'));
        assertEquals(false,Utils.isACharacterSpecialAdmitted('.'));
    }

    @Test
    public void isADigit(){
        assertEquals(true,Utils.isADigit('0'));
        assertEquals(true,Utils.isADigit('1'));
        assertEquals(true,Utils.isADigit('2'));
        assertEquals(true,Utils.isADigit('3'));
        assertEquals(true,Utils.isADigit('4'));
        assertEquals(true,Utils.isADigit('8'));
        assertEquals(false,Utils.isADigit('e'));
        assertEquals(false,Utils.isADigit('f'));
        assertEquals(false,Utils.isADigit('*'));
        assertEquals(false,Utils.isADigit(')'));
    }
}
