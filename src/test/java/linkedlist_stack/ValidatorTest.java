package linkedlist_stack;

import linkedlist_stack.exceptions.NumberOutOfRangeException;
import linkedlist_stack.exceptions.SequenceNotAllowedException;
import linkedlist_stack.exceptions.UnsupportedCharacterException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class ValidatorTest {
    Validator validator;
    Parser parser;
    Stack<Character> stackOperation;

    @BeforeEach
    public void initDependencies() {
        validator = new Validator();
        parser = new Parser();
        stackOperation = new Stack<>();
    }

    @Test
    public void validateStackSimpleTest() {
        stackOperation = parser.parseToStack("1+2");
        assertTrue(validator.validateStack(stackOperation));

        stackOperation = parser.parseToStack("27*1+4!");
        assertTrue(validator.validateStack(stackOperation));

        stackOperation = parser.parseToStack("2^3+2");
        assertTrue(validator.validateStack(stackOperation));

        stackOperation = parser.parseToStack("(2+2)!");
        assertTrue(validator.validateStack(stackOperation));

        try {
            stackOperation = parser.parseToStack("2-2+12?3");
            validator.validateStack(stackOperation);
        } catch (UnsupportedCharacterException e) {
           assertEquals("linkedlist_stack.exceptions.UnsupportedCharacterException: Your character '?' is not supported by this kind of operation",e.toString());
        }
        try {
            stackOperation = parser.parseToStack("3+6^");
            validator.validateStack(stackOperation);
         } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The input is invalid, it should not end in or start in any operator, as in your case '6^'",e.toString());
        }

        stackOperation = parser.parseToStack("7!+12*901");
        assertTrue(validator.validateStack(stackOperation));

        try {
            stackOperation = parser.parseToStack("8!9+12*12");
            validator.validateStack(stackOperation);
            assertFalse(true);
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '!9' in your input don't is admitted",e.toString());
        }

        try{
        stackOperation = parser.parseToStack("34+2^313++12");
        assertFalse(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '++' in your input don't is admitted",e.toString());
        }
    }

    @Test
    public void numberLimitTest(){
       stackOperation = parser.parseToStack("2147483647 + 0");
       assertTrue(validator.validateStack(stackOperation));

       try {
           stackOperation = parser.parseToStack("2147483648 + 0");
           validator.validateStack(stackOperation);
       }catch(NumberOutOfRangeException e){
           assertEquals("linkedlist_stack.exceptions.NumberOutOfRangeException: the inserted number '2147483648' is not admitted, it exceeds the admitted limit",e.toString());
       }
        stackOperation = parser.parseToStack("2147483647 + 1");
        assertTrue(validator.validateStack(stackOperation));
    }
    @Test
    public void validateStackIntermediateTest() {
        stackOperation = parser.parseToStack("6^(((12)))");
        assertTrue(validator.validateStack(stackOperation));

        stackOperation = parser.parseToStack("12*((12!))");
        assertTrue(validator.validateStack(stackOperation));

        stackOperation = parser.parseToStack("(12*(12)+1)");
         assertTrue(validator.validateStack(stackOperation));

        try {
            stackOperation = parser.parseToStack("3+(1+1))");
            assertFalse(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: there is an error with your parentheses inserted in your operation",e.toString());
        }

        try {
            stackOperation = parser.parseToStack("17(17*17)");
            assertFalse(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '7(' in your input don't is admitted",e.toString());
        }

        try {
            stackOperation = parser.parseToStack("3+12(12*1");
            assertFalse(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '2(' in your input don't is admitted",e.toString());
        }

        try {
            stackOperation = parser.parseToStack("1+23!+()");
            assertTrue(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: Parentheses with nothing inside are not allowed '()'",e.toString());
        }

        try {
            stackOperation = parser.parseToStack("(17+15(");
            assertFalse(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '5(' in your input don't is admitted",e.toString());
        }
    }
    @Test
    public void validateStackHardTest() {
        stackOperation = parser.parseToStack("(12+3*(12+3))");
        assertTrue(validator.validateStack(stackOperation));

        try {
            stackOperation = parser.parseToStack(")12+3*)12+3((");
            assertTrue(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters ')1' in your input don't is admitted",e.toString());
        }

        try {
            stackOperation = parser.parseToStack("(12+(17+9)*(17+18)+16!+17(17*17)");
            assertTrue(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '7(' in your input don't is admitted",e.toString());
        }


        try {
            stackOperation = parser.parseToStack("1+23()");
            assertTrue(validator.validateStack(stackOperation));
        } catch (SequenceNotAllowedException e) {
            assertEquals("linkedlist_stack.exceptions.SequenceNotAllowedException: The Order of this Characters '3(' in your input don't is admitted",e.toString());
        }
    }
}

