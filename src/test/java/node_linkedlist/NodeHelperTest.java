package node_linkedlist;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * this class tests the methods of the class NodeHelper.
 *
 * @author DanielEspinoza
 */
public class NodeHelperTest {
    Node nodeTest;

    @Test
    public void assertNodeEqualsTest() throws ExceptionDifferentNode {
        NodeHelper.assertEquals(NodeHelper.build(new int[]{1,2, 3}), NodeHelper.build(new int[]{1, 2, 3}));
        NodeHelper.assertEquals(NodeHelper.build(new int[]{17,90,1223,123123,132,839,471,12,1329,12,1912,93,1,2,3,491,1}), NodeHelper.build(new int[]{17,90,1223,123123,132,839,471,12,1329,12,1912,93,1,2,3,491,1}));

    }

    @Test
    public void BuildNodeTest() {
        nodeTest = NodeHelper.build(new int[]{1, 2, 2, 1, 1});
        Assert.assertEquals("Node = { data :1 +Node = { data :2 +Node = { data :2 +Node = { data :1 +Node = { data :1 +null}}}}}", nodeTest.toString());
        nodeTest = NodeHelper.build(new int[]{0});
        Assert.assertEquals("Node = { data :0 +null}", nodeTest.toString());
    }
}