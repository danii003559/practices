package queueClass;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ColasTest {
    Cola cola ;

    @Test
    public void colaTest(){
        cola = new Cola();
        cola.add(1);
        cola.add(2);
        cola.add(3);
        cola.add(4);
        assertEquals(1,cola.remove());
        assertEquals(2,cola.remove());
        assertEquals(3,cola.remove());
        assertEquals(4,cola.remove());
        cola.add(5);
        assertEquals(5,cola.remove());
    }
}
