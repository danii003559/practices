package task6;
import org.junit.Assert;
import task6.model.Board;
import org.junit.jupiter.api.Test;

public class PlayerTest {
    Board board = new Board();

    @Test
    public void bigBoard(){
        board.generateTable();
        board.generatePlayer();
        board.generateObstacles();
        int before = board.getObstacles()[0].getGetPositionY();
        board.moveObstacles();
        int after = board.getObstacles()[0].getGetPositionY();
        Assert.assertEquals(false,before==after);
    }
    public String comparisonBoard(String[][] board){
        String matrix= "";
        for(int i = 0 ;i<10; i++){
            for(int j = 0 ; j<10;j++){
                matrix+=board[i][j];
            }
        }
        return matrix;
    }

    @Test
    public void Board(){
        board.generateTable();
        board.generatePlayer();
        board.generateObstacles();
        String before = comparisonBoard(board.getBoard());
        board.moveObstacles();
        String after = comparisonBoard(board.getBoard());
        Assert.assertEquals(false,before.equals(after));
    }
}
