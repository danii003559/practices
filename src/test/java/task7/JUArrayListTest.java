package task7;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;


public class JUArrayListTest{
    JUArrayList juArrayList ;
    public void fillArray(int from, int upTo){
        while(from!=upTo){
            juArrayList.add(from);
            from++;
        }
    }
    @BeforeEach
    public void instance(){
        juArrayList = new JUArrayList();
    }

    @Test
    public void addTest(){
        fillArray(-6,14);
        Assert.assertArrayEquals(new Integer[]{-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13},juArrayList.toArray());
    }
    @Test
    public void addTest1(){
        fillArray(199,215);
        juArrayList.add(4,0);
        Assert.assertArrayEquals(new Integer[]{199,200,201,202,0,203,204,205,206,207,208,209,210,211,212,213,214},juArrayList.toArray());
        juArrayList.add(10,0);
        Assert.assertArrayEquals(new Integer[]{199,200,201,202,0,203,204,205,206,207,0,208,209,210,211,212,213,214},juArrayList.toArray());
    }

    @Test
    public void sizeTest(){
        fillArray(19,35);
        Assert.assertEquals(16,juArrayList.size());
    }
    @Test
    public void isEmptyTest(){
        fillArray(12,24);
        juArrayList.clear();
        Assert.assertEquals(true,juArrayList.isEmpty());
    }

    @Test
    public void getTest(){
        fillArray(-2,12);
        int numberExpected = juArrayList.get(0);
        Assert.assertEquals(-2,numberExpected);
        numberExpected = juArrayList.get(13);
        Assert.assertEquals(11,numberExpected);
        numberExpected = juArrayList.get(11);
        Assert.assertEquals(9,numberExpected);
        numberExpected = juArrayList.get(5);
        Assert.assertEquals(3,numberExpected);
    }
    @Test
    public void setTest(){
        fillArray(-2,12);
        juArrayList.set(2,12);
        juArrayList.set(7,12);
        juArrayList.set(4,12);
        juArrayList.set(8,12);
        Assert.assertArrayEquals(new Integer[]{-2,-1,12,1,12,3,4,12,12,7,8,9,10,11},juArrayList.toArray());
    }
    @Test
    public void containsTest(){
        fillArray(1,15);
        Assert.assertEquals(true,juArrayList.contains(1));
        Assert.assertEquals(false,juArrayList.contains(24));
        Assert.assertEquals(false,juArrayList.contains(-1));
        Assert.assertEquals(true,juArrayList.contains(5));
        Assert.assertEquals(true,juArrayList.contains(10));
    }

    @Test
    public void removeTest(){
        fillArray(16,27);
        juArrayList.remove(0);
        Assert.assertArrayEquals(new Integer[]{17,18,19,20,21,22,23,24,25,26},juArrayList.toArray());
        juArrayList.remove(9);
        Assert.assertArrayEquals(new Integer[]{17,18,19,20,21,22,23,24,25},juArrayList.toArray());
        juArrayList.remove(5);
        Assert.assertArrayEquals(new Integer[]{17,18,19,20,21,23,24,25},juArrayList.toArray());
        juArrayList.remove(5);
        Assert.assertArrayEquals(new Integer[]{17,18,19,20,21,24,25},juArrayList.toArray());
    }
    @Test
    public void removeObjectTest(){
        fillArray(3,8);
        fillArray(4,7);
        Integer numberToDelete = 5;
        Assert.assertEquals(true,juArrayList.remove(numberToDelete));
        Assert.assertArrayEquals(new Integer[]{3,4,6,7,4,5,6},juArrayList.toArray());
        numberToDelete = 6;
        Assert.assertEquals(true,juArrayList.remove(numberToDelete));
        Assert.assertArrayEquals(new Integer[]{3,4,7,4,5,6},juArrayList.toArray());
    }
    @Test
    public void iteratorTest(){
        fillArray(3,8);
        Iterator<Integer> iteratorNumbers =juArrayList.iterator();
        Assert.assertEquals(true,iteratorNumbers.hasNext());
        int  number = iteratorNumbers.next();
        Assert.assertEquals(3,number);
        number = iteratorNumbers.next();
        Assert.assertEquals(4,number);
    }

}


























